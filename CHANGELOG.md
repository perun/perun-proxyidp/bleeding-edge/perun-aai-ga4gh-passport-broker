## [2.1.12](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/compare/v2.1.11...v2.1.12) (2025-03-06)


### Bug Fixes

* perun-ga4gh-broker.war path 404 error ([d7a5a40](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/commit/d7a5a4051ac5a57587f3fb364e3ca2aca11922f5))

## [2.1.11](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/compare/v2.1.10...v2.1.11) (2025-03-06)


### Bug Fixes

* timeout on the establishing connection with repositories ([b0ba06a](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/commit/b0ba06a112807723ff725957d3234aec03f5b59a))

## [2.1.10](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/compare/v2.1.9...v2.1.10) (2023-12-29)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.17 ([c25614c](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/commit/c25614c56293a04a0ac0707ba4e1af02b3dcb607))
* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.18 ([c217781](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/commit/c2177817b82b4a54cc672aaaafc4ee89d6aab78a))

## [2.1.9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/compare/v2.1.8...v2.1.9) (2023-10-12)


### Bug Fixes

* **deps:** update dependency org.apache.directory.api:api-all to v2.1.5 ([6f08036](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/commit/6f08036c0f77b972b4dead304bdf10e41f21086a))

## [2.1.8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/compare/v2.1.7...v2.1.8) (2023-09-24)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.16 ([b2f4582](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/commit/b2f45821db6f65ca6d8e4e842da799e3d37d765e))

## [2.1.7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/compare/v2.1.6...v2.1.7) (2023-08-29)


### Bug Fixes

* 🐛 Linked Identities visa value separator ([5397692](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/commit/539769204c4ccedd23fee08bf9c7059bfb515923))

## [2.1.6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/compare/v2.1.5...v2.1.6) (2023-08-29)


### Bug Fixes

* **deps:** update dependency org.apache.directory.api:api-all to v2.1.4 ([b3d0223](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/commit/b3d0223cecaab93e8ec6cb0c3af73854a6fbc80c))

## [2.1.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/compare/v2.1.4...v2.1.5) (2023-08-27)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.15 ([f6ef4f9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/commit/f6ef4f967a360bcce8d7b79dac504acd4f1b5901))

## [2.1.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/compare/v2.1.3...v2.1.4) (2023-07-23)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.14 ([07aaadd](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/commit/07aaaddad08d72164972d5209a644d36240ccf50))

## [2.1.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/compare/v2.1.2...v2.1.3) (2023-06-25)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.13 ([13fdb23](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/commit/13fdb23bf1977c98d2953742b0cfe89294f8d8d8))

## [2.1.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/compare/v2.1.1...v2.1.2) (2023-05-21)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.12 ([31ab344](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/commit/31ab34410be51fcb598446387e7b62f010af78be))

## [2.1.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/compare/v2.1.0...v2.1.1) (2023-05-03)


### Bug Fixes

* 🐛 Fix loading conditional beans ([c68ac88](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/commit/c68ac8806b461c777de0ae168823447e48a7df74))

# [2.1.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/compare/v2.0.7...v2.1.0) (2023-05-02)


### Features

* 🎸 Mocked version implemented ([c5c6452](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/commit/c5c645245fd46f347071c2f21e657d1e9f3645c4))

## [2.0.7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/compare/v2.0.6...v2.0.7) (2023-04-23)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.11 ([094595c](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/commit/094595ca9e1f9631385df26748d031769aa8b9d6))

## [2.0.6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/compare/v2.0.5...v2.0.6) (2023-04-09)


### Bug Fixes

* **deps:** update dependency org.apache.directory.api:api-all to v2.1.3 ([052e18b](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/commit/052e18ba9bf4bdc4e9fac4321ac64434c8c8fc06))

## [2.0.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/compare/v2.0.4...v2.0.5) (2023-03-26)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.10 ([882f0a1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/commit/882f0a1b7201f3199dca2113090a414ac2be72b8))

## [2.0.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/compare/v2.0.3...v2.0.4) (2023-03-18)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.9 ([f5714d0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/commit/f5714d07d308717314a5071d22284f6cb1d3c0ae))

## [2.0.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/compare/v2.0.2...v2.0.3) (2023-01-22)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.8 ([7bd11f3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/commit/7bd11f35a9ebe07ea54722ce9b743bfae1321f28))

## [2.0.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/compare/v2.0.1...v2.0.2) (2022-12-25)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.7 ([0695386](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/commit/06953865a6fd99158ca000af97c25d02f4dd1069))

## [2.0.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/compare/v2.0.0...v2.0.1) (2022-12-06)


### Bug Fixes

* 🐛 Add missing JKU to Passport token ([e2feec7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/commit/e2feec7c13c64aa08b3d2be1e6836a5109006ce9))

# [2.0.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/compare/v1.0.7...v2.0.0) (2022-12-06)


### Features

* 🎸 Implemented fetching via v1.2 spec ([93c34e6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/commit/93c34e6f977aa18eb998c9cd8fcd9aeaff526bfa))


### BREAKING CHANGES

* 🧨 new endpoints mapping

## [1.0.7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/compare/v1.0.6...v1.0.7) (2022-11-27)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.6 ([a264720](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/commit/a2647202ebf8aa65a54b54895814e9eeabe5337e))

## [1.0.6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/compare/v1.0.5...v1.0.6) (2022-10-23)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.5 ([30b503c](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/commit/30b503cc240105d96fd9d7d9745a7c3ce3526b94))

## [1.0.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/compare/v1.0.4...v1.0.5) (2022-10-06)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.4 ([93defae](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/commit/93defae3f9aabca95113e83aeff527367cc5c24b))

## [1.0.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/compare/v1.0.3...v1.0.4) (2022-10-05)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.4 ([041ba39](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/commit/041ba39993003efdd57c04421c9e49796fd6d251))

## [1.0.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/compare/v1.0.2...v1.0.3) (2022-09-28)


### Bug Fixes

* first release in GitLab ([b9034e9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/perun-aai-ga4gh-passport-broker/commit/b9034e9cb5f80b5c13fb71b6ec9d169f9ae1141e))

## [1.0.2](https://github.com/CESNET/perun-aai-ga4gh-passport-broker/compare/v1.0.1...v1.0.2) (2022-08-22)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.3 ([65936f9](https://github.com/CESNET/perun-aai-ga4gh-passport-broker/commit/65936f9b327b8f0cfc4be4e5e3f8518b2e3d199d))

## [1.0.1](https://github.com/CESNET/perun-aai-ga4gh-passport-broker/compare/v1.0.0...v1.0.1) (2022-08-19)


### Bug Fixes

* **deps:** update dependency org.apache.directory.api:api-all to v2.1.2 ([e9c76ef](https://github.com/CESNET/perun-aai-ga4gh-passport-broker/commit/e9c76efc01d74313893bc0cfc27d2bcea3dc0915))

# 1.0.0 (2022-08-16)


### Bug Fixes

* **deps:** update dependency org.apache.directory.api:api-all to v2.1.1 ([4543000](https://github.com/CESNET/perun-aai-ga4gh-passport-broker/commit/45430003df6332ea3c7f84458e9cc669f7373820))
