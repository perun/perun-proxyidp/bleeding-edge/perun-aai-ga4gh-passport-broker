package cz.muni.ics.ga4gh.web.configuration.properties;

import cz.muni.ics.ga4gh.base.credentials.OAuth2ResourceServerCredentials;
import javax.annotation.PostConstruct;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.validation.annotation.Validated;

@Getter
@ToString
@Slf4j

@Validated
@ConstructorBinding
@ConfigurationProperties(prefix = "passport-v1-2")
@ConditionalOnProperty(prefix = "passport-v1-2", value = "enabled", havingValue = "True")
public class V1_2EndpointProperties {

    @NotNull
    private final OAuth2ResourceServerCredentials resourceServerCredentials;

    public V1_2EndpointProperties(@NotEmpty OAuth2ResourceServerCredentials auth) {
        this.resourceServerCredentials = auth;
    }

    @PostConstruct
    public void init() {
        log.info("Initialized '{}' properties", this.getClass().getSimpleName());
        log.debug("{}", this);
    }

}
