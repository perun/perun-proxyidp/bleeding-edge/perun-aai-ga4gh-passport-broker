package cz.muni.ics.ga4gh.web.configuration.security;

import cz.muni.ics.ga4gh.base.credentials.BasicAuthCredentials;
import cz.muni.ics.ga4gh.base.properties.VisaWritersProperties;
import cz.muni.ics.ga4gh.web.configuration.properties.VisaWriterAuthProperties;
import cz.muni.ics.ga4gh.web.configuration.security.auth.ApiKeyAuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.InMemoryUserDetailsManagerConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


@Order(3)
@Configuration
@ConditionalOnBean(VisaWritersProperties.class)
public class VisaWriterEndpointWebSecurityConfigurer {

    public static final String PATH_VISAS = "/visas";

    public static final String ROLE_WRITER = "ROLE_WRITER";

    private final PasswordEncoder passwordEncoder;

    private final VisaWriterAuthProperties authProperties;

    @Autowired
    public VisaWriterEndpointWebSecurityConfigurer(VisaWriterAuthProperties authProperties,
                                                   PasswordEncoder passwordEncoder)
    {
        this.authProperties = authProperties;
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        InMemoryUserDetailsManagerConfigurer<AuthenticationManagerBuilder> configurer =
            auth.inMemoryAuthentication();

        for (BasicAuthCredentials credentials: authProperties.getBasicAuth()) {
            configurer.withUser(credentials.getUsername())
                .password(passwordEncoder.encode(credentials.getPassword()))
                .authorities(ROLE_WRITER);
        }
    }

    @Bean
    public SecurityFilterChain authenticationFilterChain(HttpSecurity http) throws Exception {
        ApiKeyAuthenticationFilter apiKeyAuthenticationFilter = new ApiKeyAuthenticationFilter(
            authProperties.getApiKeys());

        http.antMatcher(PATH_VISAS + "/**")
            .addFilterBefore(apiKeyAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)
            .authorizeRequests(configurer -> {
                configurer.antMatchers(PATH_VISAS + "/**").authenticated();
            })
            .httpBasic()
                .and()
            .sessionManagement(
                session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            )
            .csrf().disable()
            .exceptionHandling();

        http.headers().frameOptions().sameOrigin();
        return http.build();
    }

}
