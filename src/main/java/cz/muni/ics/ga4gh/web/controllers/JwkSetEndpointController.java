package cz.muni.ics.ga4gh.web.controllers;

import static cz.muni.ics.ga4gh.web.controllers.JwkSetEndpointController.PATH_JWK;

import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.JWKSet;
import cz.muni.ics.ga4gh.middleware.service.JWTSigningService;
import java.util.ArrayList;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(originPatterns = "*")
@RestController
@RequestMapping(value = PATH_JWK)
public class JwkSetEndpointController {

    public static final String PATH_JWK = "/jwk";

    private final JWTSigningService jwtService;

    @Autowired
    public JwkSetEndpointController(JWTSigningService jwtService) {
        this.jwtService = jwtService;
    }

    @RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public String getJwk() {
        // map from key id to key
        Map<String, JWK> keys = jwtService.getPublicKeys();
        JWKSet jwkSet = new JWKSet(new ArrayList<>(keys.values()));
        return jwkSet.toString();
    }
}
