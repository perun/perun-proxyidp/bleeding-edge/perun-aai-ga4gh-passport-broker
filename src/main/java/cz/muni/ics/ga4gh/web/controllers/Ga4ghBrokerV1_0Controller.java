package cz.muni.ics.ga4gh.web.controllers;

import static cz.muni.ics.ga4gh.web.configuration.security.PassportEndpointV1_0WebSecurityConfigurer.PATH_V_1_0;

import com.fasterxml.jackson.databind.JsonNode;
import cz.muni.ics.ga4gh.middleware.facade.Ga4ghBrokerFacade;
import cz.muni.ics.ga4gh.middleware.service.exception.UserNotFoundException;
import cz.muni.ics.ga4gh.middleware.service.exception.UserNotUniqueException;
import cz.muni.ics.ga4gh.web.configuration.security.PassportEndpointV1_0WebSecurityConfigurer;
import cz.muni.ics.ga4gh.web.exception.InvalidRequestParametersException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


@ConditionalOnBean(PassportEndpointV1_0WebSecurityConfigurer.class)
@RestController
public class Ga4ghBrokerV1_0Controller {

    private final Ga4ghBrokerFacade ga4ghBrokerFacade;

    @Autowired
    public Ga4ghBrokerV1_0Controller(Ga4ghBrokerFacade ga4ghBrokerFacade) {
        this.ga4ghBrokerFacade = ga4ghBrokerFacade;
    }

    @GetMapping(value = PATH_V_1_0 + "/{user_id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public JsonNode getGa4ghPassport(@PathVariable(name = "user_id") String userIdentifier)
        throws UserNotFoundException, UserNotUniqueException, InvalidRequestParametersException
    {
        if (!StringUtils.hasText(userIdentifier)) {
            throw new InvalidRequestParametersException("No user identifier specified");
        }
        return ga4ghBrokerFacade.getGa4ghPassportClaim(userIdentifier);
    }

}
