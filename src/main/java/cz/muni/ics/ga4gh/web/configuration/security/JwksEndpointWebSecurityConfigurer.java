package cz.muni.ics.ga4gh.web.configuration.security;

import static cz.muni.ics.ga4gh.web.controllers.JwkSetEndpointController.PATH_JWK;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class JwksEndpointWebSecurityConfigurer {

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.antMatcher(PATH_JWK)
            .authorizeRequests(
                configurer -> configurer.antMatchers(PATH_JWK).permitAll())
            .sessionManagement(
                session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            )
            .exceptionHandling();
        return http.build();
    }

}
