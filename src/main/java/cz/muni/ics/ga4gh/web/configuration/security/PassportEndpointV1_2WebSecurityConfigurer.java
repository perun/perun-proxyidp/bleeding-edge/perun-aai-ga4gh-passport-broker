package cz.muni.ics.ga4gh.web.configuration.security;


import static cz.muni.ics.ga4gh.base.model.Ga4ghPassportClaim.GA4GH_PASSPORT_V1;

import cz.muni.ics.ga4gh.base.credentials.OAuth2ResourceServerCredentials;
import cz.muni.ics.ga4gh.web.configuration.properties.V1_2EndpointProperties;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;

@ConditionalOnBean(V1_2EndpointProperties.class)
@Configuration
@Order(2)
public class PassportEndpointV1_2WebSecurityConfigurer {

    public static final String PATH_V_1_2 = "/1.2";

    private final OAuth2ResourceServerCredentials OAuth2ResourceServer;

    @Autowired
    public PassportEndpointV1_2WebSecurityConfigurer(@NotNull V1_2EndpointProperties properties) {
        this.OAuth2ResourceServer = properties.getResourceServerCredentials();
    }

    @Bean
    public SecurityFilterChain oauth2FilterChain(HttpSecurity http) throws Exception {
        http.antMatcher(PATH_V_1_2 + "/**")
            .authorizeRequests(c -> {
                c.antMatchers(PATH_V_1_2 + "/**").hasAuthority("SCOPE_" + GA4GH_PASSPORT_V1);
            })
            .oauth2ResourceServer(oauth2 -> {
                oauth2.opaqueToken(token -> {
                    token.introspectionUri(OAuth2ResourceServer.getIntrospectionUri());
                    token.introspectionClientCredentials(
                        OAuth2ResourceServer.getIntrospectionClientId(),
                        OAuth2ResourceServer.getIntrospectionClientSecret()
                    );
                });
            })
            .sessionManagement(
                session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            )
            .exceptionHandling();
        return http.build();
    }

}
