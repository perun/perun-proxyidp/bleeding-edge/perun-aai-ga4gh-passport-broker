package cz.muni.ics.ga4gh.persistence.database;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import cz.muni.ics.ga4gh.Application;
import cz.muni.ics.ga4gh.base.properties.VisaWritersProperties;
import cz.muni.ics.ga4gh.persistence.database.annotation.VisaDatabaseStorage;
import cz.muni.ics.ga4gh.persistence.database.properties.DatabaseProperties;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Slf4j
@Configuration
@EnableJpaRepositories
@EnableTransactionManagement
@ConditionalOnBean(VisaWritersProperties.class)
public class VisaDatabaseStorageConfiguration implements InitializingBean {

    @Override
    public void afterPropertiesSet() {
        log.info("Initialized '{}'", this.getClass().getSimpleName());
        log.debug("{}", this);
    }

    @Bean
    @VisaDatabaseStorage
    public DatabaseProperties databaseProperties(VisaWritersProperties visaWritersProperties) {
        return visaWritersProperties.getDatabaseProperties();
    }

    @Bean
    @VisaDatabaseStorage
    public DataSource dataSource(@VisaDatabaseStorage DatabaseProperties databaseProperties) {
        HikariConfig configuration = new HikariConfig();
        configuration.setJdbcUrl(databaseProperties.getUrl());
        configuration.setUsername(databaseProperties.getUsername());
        configuration.setPassword(databaseProperties.getPassword());
        configuration.setDriverClassName(databaseProperties.getDriver());
        configuration.setMaximumPoolSize(databaseProperties.getMaxPoolSize());

        return new HikariDataSource(configuration);
    }

    @VisaDatabaseStorage
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
        @VisaDatabaseStorage DataSource dataSource)
    {
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setGenerateDdl(true);

        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setJpaVendorAdapter(vendorAdapter);
        factory.setPackagesToScan(Application.class.getPackageName());
        factory.setDataSource(dataSource);
        return factory;
    }

    @Bean
    public PlatformTransactionManager transactionManager(
        @VisaDatabaseStorage EntityManagerFactory entityManagerFactory)
    {
        JpaTransactionManager txManager = new JpaTransactionManager();
        txManager.setEntityManagerFactory(entityManagerFactory);
        return txManager;
    }

}
