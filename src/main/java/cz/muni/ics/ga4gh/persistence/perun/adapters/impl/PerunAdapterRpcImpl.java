package cz.muni.ics.ga4gh.persistence.perun.adapters.impl;

import static cz.muni.ics.ga4gh.persistence.perun.enums.MemberStatus.VALID;

import com.fasterxml.jackson.databind.JsonNode;
import cz.muni.ics.ga4gh.base.model.Affiliation;
import cz.muni.ics.ga4gh.persistence.perun.adapters.PerunAdapterRpc;
import cz.muni.ics.ga4gh.persistence.perun.adapters.PerunRpcMapper;
import cz.muni.ics.ga4gh.persistence.perun.connectors.PerunConnectorRpc;
import cz.muni.ics.ga4gh.persistence.perun.model.Group;
import cz.muni.ics.ga4gh.persistence.perun.model.Member;
import cz.muni.ics.ga4gh.persistence.perun.model.PerunAttributeValue;
import cz.muni.ics.ga4gh.persistence.perun.model.UserExtSource;
import cz.muni.ics.ga4gh.persistence.perun.properties.PerunRpcConnectorProperties;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
@Slf4j
@ConditionalOnBean({PerunConnectorRpc.class, PerunRpcConnectorProperties.class})
public class PerunAdapterRpcImpl implements PerunAdapterRpc {

    public static final String EXT_SOURCE_IDP = "cz.metacentrum.perun.core.impl.ExtSourceIdp";
    public static final String EXT_LOGIN = "extLogin";
    public static final String EXT_SOURCE_NAME = "extSourceName";
    public static final String USER_EXT_SOURCE = "userExtSource";
    public static final String ATTR_NAMES = "attributeNames";
    public static final String VALUE_CREATED_AT = "valueCreatedAt";

    public static final String ID = "id";
    public static final String VO = "vo";
    public static final String USER = "user";
    public static final String MEMBER = "member";
    public static final String GROUP = "group";
    public static final String PARENT_GROUP = "parentGroup";

    public static final String ATTRIBUTE_NAME = "attributeName";
    public static final String ATTRIBUTE_VALUE = "attributeValue";

    public static final String ATTRIBUTES_MANAGER = "attributesManager";
    public static final String GROUPS_MANAGER = "groupsManager";
    public static final String MEMBERS_MANAGER = "membersManager";
    public static final String USERS_MANAGER = "usersManager";

    public static final String GET_USER_BY_EXT_SOURCE_NAME_AND_EXT_LOGIN = "getUserByExtSourceNameAndExtLogin";
    public static final String GET_USER_EXT_SOURCES = "getUserExtSources";
    public static final String GET_GROUP_BY_ID = "getGroupById";
    public static final String GET_MEMBER_BY_USER = "getMemberByUser";
    public static final String GET_MEMBERS_BY_USER = "getMembersByUser";
    public static final String GET_MEMBER_GROUPS = "getMemberGroups";
    public static final String IS_GROUP_MEMBER = "isGroupMember";
    public static final String GET_ATTRIBUTE = "getAttribute";
    public static final String GET_ATTRIBUTES = "getAttributes";
    public static final String GET_USERS_BY_ATTRIBUTE_VALUE = "getUsersByAttributeValue";
    public static final String GET_SUB_GROUPS = "getSubGroups";

    private final PerunConnectorRpc connectorRpc;
    public PerunAdapterRpcImpl(PerunConnectorRpc connectorRpc) {
        this.connectorRpc = connectorRpc;
    }

    @Override
    public Long getPerunUserId(String extLogin, String extSourceName) {
        if (!StringUtils.hasText(extLogin)) {
            throw new IllegalArgumentException("Empty extLogin passed");
        } else if (!StringUtils.hasText(extSourceName)) {
            throw new IllegalArgumentException("Empty extSourceName passed");
        }

        Map<String, Object> map = new LinkedHashMap<>();
        map.put(EXT_LOGIN, extLogin);
        map.put(EXT_SOURCE_NAME, extSourceName);

        JsonNode response = connectorRpc.post(USERS_MANAGER, GET_USER_BY_EXT_SOURCE_NAME_AND_EXT_LOGIN, map);

        return response.get(ID) == null ? null : response.get(ID).asLong();
    }

    @Override
    public boolean isUserInGroup(Long userId, Long groupId) {
        if (userId == null) {
            throw new IllegalArgumentException("Null user ID passed");
        } else if (groupId == null) {
            throw new IllegalArgumentException("Null group ID passed");
        }


        Map<String, Object> groupParams = new LinkedHashMap<>();
        groupParams.put(ID, groupId);
        JsonNode groupResponse = connectorRpc.post(GROUPS_MANAGER, GET_GROUP_BY_ID, groupParams);
        Group group = PerunRpcMapper.mapGroup(groupResponse);

        Map<String, Object> memberParams = new LinkedHashMap<>();
        memberParams.put(VO, group.getVoId());
        memberParams.put(USER, userId);
        JsonNode memberResponse = connectorRpc.post(MEMBERS_MANAGER, GET_MEMBER_BY_USER, memberParams);
        Member member = PerunRpcMapper.mapMember(memberResponse);

        Map<String, Object> isGroupMemberParams = new LinkedHashMap<>();
        isGroupMemberParams.put(GROUP, groupId);
        isGroupMemberParams.put(MEMBER, member.getId());
        JsonNode res = connectorRpc.post(GROUPS_MANAGER, IS_GROUP_MEMBER, isGroupMemberParams);

        return res.asBoolean(false);
    }

    @Override
    public List<Affiliation> getGroupAffiliations(Long userId, String groupAffiliationsAttr) {
        if (userId == null) {
            throw new IllegalArgumentException("Null user ID passed");
        } else if (!StringUtils.hasText(groupAffiliationsAttr)) {
            throw new IllegalArgumentException("Empty group affiliations attribute name passed");
        }


        List<Affiliation> affiliations = new ArrayList<>();
        List<Member> userMembers = getMembersByUser(userId);

        for (Member member : userMembers) {
            if (!VALID.equals(member.getStatus())) {
                continue;
            }
            addAffiliationsFromGroupsForMember(groupAffiliationsAttr, affiliations, member);
        }

        return affiliations;
    }

    @Override
    public List<Affiliation> getGroupAffiliations(Long userId, Long voId,
                                                  String groupAffiliationsAttr)
    {
        if (userId == null) {
            throw new IllegalArgumentException("Null user ID passed");
        } else if (voId == null) {
            throw new IllegalArgumentException("Null vo ID passed");
        } else if (!StringUtils.hasText(groupAffiliationsAttr)) {
            throw new IllegalArgumentException("Empty group affiliations attribute name passed");
        }

        List<Affiliation> affiliations = new ArrayList<>();
        List<Member> userMembers = getMembersByUser(userId);

        for (Member member : userMembers) {
            if (!VALID.equals(member.getStatus())) {
                continue;
            } else if (!Objects.equals(voId, member.getVoId())) {
                continue;
            }
            addAffiliationsFromGroupsForMember(groupAffiliationsAttr, affiliations, member);
        }

        return affiliations;
    }

    @Override
    public String getUserAttributeCreatedAt(Long userId, String attributeName) {
        if (userId == null) {
            throw new IllegalArgumentException("Null user ID passed");
        } else if (!StringUtils.hasText(attributeName)) {
            throw new IllegalArgumentException("Empty attribute name passed");
        }

        Map<String, Object> map = new LinkedHashMap<>();
        map.put(USER, userId);
        map.put(ATTRIBUTE_NAME, attributeName);

        JsonNode res = connectorRpc.post(ATTRIBUTES_MANAGER, GET_ATTRIBUTE, map);
        if (res == null || !res.hasNonNull(VALUE_CREATED_AT)) {
            return null;
        }
        return res.get(VALUE_CREATED_AT).asText();
    }

    @Override
    public Set<Affiliation> getUserExtSourcesAffiliations(Long userId,
                                                           String affiliationsAttr)
    {
        if (userId == null) {
            throw new IllegalArgumentException("Null user ID passed");
        } else if (!StringUtils.hasText(affiliationsAttr)) {
            throw new IllegalArgumentException("Empty affiliations attr name passed");
        }

        List<UserExtSource> userExtSources = getUserExtSources(userId);
        if (userExtSources.isEmpty()) {
            return new HashSet<>();
        }

        Set<Affiliation> affiliations = new HashSet<>();
        for (UserExtSource ues : userExtSources) {
            if (!EXT_SOURCE_IDP.equals(ues.getExtSource().getType())) {
                continue;
            }
            PerunAttributeValue uesAttrValue = getUserExtSourceAttributeValue(
                ues.getId(), affiliationsAttr);

            long asserted = ues.getLastAccess().getTime() / 1000L;

            String affs = uesAttrValue.valueAsString();

            if (affs != null) {
                for (String aff : affs.split(";")) {
                    Affiliation affiliation = new Affiliation(
                        ues.getExtSource().getName(), aff, asserted);
                    log.debug("found {} from IdP {} asserted at {}",
                        aff, ues.getExtSource().getName(),asserted);
                    affiliations.add(affiliation);
                }
            }
        }

        return affiliations;
    }

    @Override
    public Set<Long> getUserIdsByAttributeValue(String attributeName, String attrValue) {
        if (!StringUtils.hasText(attributeName)) {
            throw new IllegalArgumentException("Empty attribute name passed");
        } else if (!StringUtils.hasText(attrValue)) {
            throw new IllegalArgumentException("Empty attribute value passed");
        }

        Set<Long> result = new HashSet<>();
        Map<String, Object> map = new LinkedHashMap<>();
        map.put(ATTRIBUTE_NAME, attributeName);
        map.put(ATTRIBUTE_VALUE, attrValue);

        JsonNode res = connectorRpc.post(USERS_MANAGER, GET_USERS_BY_ATTRIBUTE_VALUE, map);
        if (res != null) {
            for (int i = 0; i < res.size(); i++) {
                result.add(res.get(i).get(ID).asLong());
            }
        }
        return result;
    }

    @Override
    public String getUserSub(Long userId, String attributeName) {
        if (userId == null) {
            throw new IllegalArgumentException("Null user ID passed");
        } else if (!StringUtils.hasText(attributeName)) {
            throw new IllegalArgumentException("Empty sub attribute name passed");
        }

        Map<String, Object> map = new LinkedHashMap<>();
        map.put(USER, userId);
        map.put(ATTRIBUTE_NAME, attributeName);
        JsonNode res = connectorRpc.post(ATTRIBUTES_MANAGER, GET_ATTRIBUTE, map);

        PerunAttributeValue value = PerunRpcMapper.mapAttributeValue(res);
        if (value != null) {
            return value.getValue().textValue();
        } else {
            return null;
        }
    }
    @Override
    public boolean isUserInVo(Long userId, Long voId) {
        Member member = getMemberByUser(userId, voId);
        return member != null && member.getStatus() == VALID;
    }

    @Override
    public List<UserExtSource> getIdpUserExtSources(Long userId) {
        if (userId == null) {
            throw new IllegalArgumentException("Null user ID passed");
        }

        List<UserExtSource> userExtSources = getUserExtSources(userId);
        if (userExtSources.isEmpty()) {
            return new ArrayList<>();
        }
        userExtSources = userExtSources.stream()
            .filter(ues -> EXT_SOURCE_IDP.equals(ues.getExtSource().getType()))
            .collect(Collectors.toList());
        return userExtSources;
    }

    @Override
    public Set<Affiliation> getGroupAffiliations(Long userId,
                                                 Long voId,
                                                 Long rootGroupId,
                                                 String affiliationsAttr,
                                                 String orgUrlAttr,
                                                 String groupMemberExpirationAttr)
    {
        if (userId == null) {
            throw new IllegalArgumentException("Null user ID passed");
        } else if (voId == null) {
            throw new IllegalArgumentException("Null vo ID passed");
        } else if (rootGroupId == null) {
            throw new IllegalArgumentException("Null vo ID passed");
        } else if (!StringUtils.hasText(affiliationsAttr)) {
            throw new IllegalArgumentException("Empty group affiliations attribute name passed");
        } else if (!StringUtils.hasText(orgUrlAttr)) {
            throw new IllegalArgumentException("Empty group affiliations attribute name passed");
        }
        //TODO: uncomment when want to use the attribute, at the moment it is not necessary
        //else if (!StringUtils.hasText(groupMemberExpirationAttr)) {
        //    throw new IllegalArgumentException("Empty group affiliations attribute name passed");
        //}

        Set<Affiliation> affiliations = new HashSet<>();

        Member userMember = getMemberByUser(userId, voId);
        if (userMember == null || userMember.getStatus() != VALID) {
            // user is not a VALID member in the VO, cannot be a VALID member in groups
            return affiliations;
        }
        List<Group> memberGroups = getMemberGroups(userMember.getId());
        if (memberGroups.isEmpty()) {
            // member in no groups, cannot have affiliations
            return affiliations;
        }

        List<Group> subGroups = getSubGroups(rootGroupId);
        if (subGroups.isEmpty()) {
            // no subgroups, cannot have affiliation (probably noone uses it)
            return affiliations;
        }

        Set<Long> groupIdsWhereMember = memberGroups.stream().map(Group::getId).collect(Collectors.toSet());
        Set<Long> subgroupIds = subGroups.stream().map(Group::getId).collect(Collectors.toSet());
        groupIdsWhereMember.retainAll(subgroupIds);
        if (groupIdsWhereMember.size() > 0) {
            for (Long gid: groupIdsWhereMember) {
                Map<String, PerunAttributeValue> attrValues = getGroupAttributeValues(
                    gid, List.of(affiliationsAttr, orgUrlAttr)
                );
                PerunAttributeValue groupAffiliations = attrValues.getOrDefault(affiliationsAttr, null);
                PerunAttributeValue orgUrl = attrValues.getOrDefault(orgUrlAttr, null);

                //TODO needs fixing - in case of indirect membership this attribute is not set
                //PerunAttributeValue memberExpiration = getGroupMemberAttributeValue(gid,
                //    userMember.getId(), groupMemberExpirationAttr);

                if (groupAffiliations != null && groupAffiliations.valueAsList() != null
                    && orgUrl != null && StringUtils.hasText(orgUrl.valueAsString())
                    //TODO: uncomment if want to use the attribute
                    //&& memberExpiration != null && StringUtils.hasText(memberExpiration.valueAsString())
                ) {
                    long asserted = System.currentTimeMillis() / 1000L;
                    //TODO still not the best thing - we need to know when they got the affiliation,
                    // not when it expires and estimate when they got it
//                    if (memberExpiration != null && StringUtils.hasText(memberExpiration.valueAsString())) {
//                        try {
//                            asserted = LocalDate.parse(memberExpiration.valueAsString(),
//                                    DateTimeFormatter.ISO_LOCAL_DATE)
//                                .minusYears(1).toEpochSecond(
//                                    LocalTime.MIDNIGHT, ZoneOffset.UTC);
//                        } catch (DateTimeParseException ex) {
//                            log.warn("Could not parse asserted for group affiliation ({})",
//                                memberExpiration.valueAsString());
//                        }
//                    }
                    for (String value : groupAffiliations.valueAsList()) {
                        Affiliation affiliation = new Affiliation(orgUrl.valueAsString(), value, asserted);
                        log.debug("found {} on group {}", value, gid);
                        affiliations.add(affiliation);
                    }
                }
            }
        }
        return affiliations;
    }

    private Map<String, PerunAttributeValue> getGroupAttributeValues(Long group,
                                                                     List<String> attrNames)
    {
        if (group == null) {
            throw new IllegalArgumentException("Null group ID passed");
        } else if (attrNames == null || attrNames.isEmpty()) {
            throw new IllegalArgumentException("Null or empty list of attribute names passed");
        }

        Map<String, Object> map = new LinkedHashMap<>();

        map.put(GROUP, group);
        map.put(ATTR_NAMES, attrNames);

        JsonNode response = connectorRpc.post(ATTRIBUTES_MANAGER, GET_ATTRIBUTES, map);

        return PerunRpcMapper.mapAttributes(response);
    }

    private List<Group> getSubGroups(Long parentGroup) {
        if (parentGroup == null) {
            throw new IllegalArgumentException("Null parent group ID passed");
        }
        Map<String, Object> params = new LinkedHashMap<>();
        params.put(PARENT_GROUP, parentGroup);
        JsonNode jsonNode = connectorRpc.post(GROUPS_MANAGER, GET_SUB_GROUPS, params);

        return PerunRpcMapper.mapGroups(jsonNode);
    }

    private Member getMemberByUser(Long userId, Long voId) {
        if (userId == null) {
            throw new IllegalArgumentException("Null user ID passed");
        } else if (voId == null) {
            throw new IllegalArgumentException("Null vo ID passed");
        }
        Map<String, Object> params = new LinkedHashMap<>();
        params.put(USER, userId);
        params.put(VO, voId);
        JsonNode jsonNode = connectorRpc.post(MEMBERS_MANAGER, GET_MEMBER_BY_USER, params);

        return PerunRpcMapper.mapMember(jsonNode);
    }

    private List<Member> getMembersByUser(Long userId) {
        if (userId == null) {
            throw new IllegalArgumentException("Null user ID passed");
        }
        Map<String, Object> params = new LinkedHashMap<>();
        params.put(USER, userId);
        JsonNode jsonNode = connectorRpc.post(MEMBERS_MANAGER, GET_MEMBERS_BY_USER, params);

        return PerunRpcMapper.mapMembers(jsonNode);
    }

    private List<Group> getMemberGroups(Long memberId) {
        if (memberId == null) {
            throw new IllegalArgumentException("Null member ID passed");
        }

        Map<String, Object> map = new LinkedHashMap<>();
        map.put(MEMBER, memberId);

        JsonNode response = connectorRpc.post(GROUPS_MANAGER, GET_MEMBER_GROUPS, map);
        return PerunRpcMapper.mapGroups(response);
    }

    private PerunAttributeValue getGroupAttributeValue(Group group, String attributeName) {
        if (group == null || group.getId() == null) {
            throw new IllegalArgumentException("Null group or group with null ID passed");
        } else if (!StringUtils.hasText(attributeName)) {
            throw new IllegalArgumentException("Empty attribute name passed");
        }
        Map<String, Object> map = new LinkedHashMap<>();
        map.put(GROUP, group.getId());
        map.put(ATTRIBUTE_NAME, attributeName);
        JsonNode res = connectorRpc.post(ATTRIBUTES_MANAGER, GET_ATTRIBUTE, map);

        return PerunRpcMapper.mapAttributeValue(res);
    }

    private List<UserExtSource> getUserExtSources(Long userId) {
        if (userId == null) {
            throw new IllegalArgumentException("Null user ID passed");
        }
        Map<String, Object> map = new LinkedHashMap<>();
        map.put(USER, userId);

        JsonNode response = connectorRpc.post(USERS_MANAGER, GET_USER_EXT_SOURCES, map);
        return PerunRpcMapper.mapUserExtSources(response);
    }

    private PerunAttributeValue getUserExtSourceAttributeValue(Long uesId, String attributeName)
    {
        if (uesId == null) {
            throw new IllegalArgumentException("Null user ext source ID passed");
        } else if (!StringUtils.hasText(attributeName)) {
            throw new IllegalArgumentException("Null or empty attribute name passed");
        }

        Map<String, Object> map = new LinkedHashMap<>();
        map.put(USER_EXT_SOURCE, uesId);
        map.put(ATTRIBUTE_NAME, attributeName);

        JsonNode response = connectorRpc.post(ATTRIBUTES_MANAGER, GET_ATTRIBUTE, map);

        return PerunRpcMapper.mapAttributeValue(response);
    }

    private PerunAttributeValue getGroupMemberAttributeValue(Long group, Long member, String attributeName) {
        if (group == null) {
            throw new IllegalArgumentException("Null group ID passed");
        } else if (member == null) {
            throw new IllegalArgumentException("Null member ID passed");
        } else if (!StringUtils.hasText(attributeName)) {
            throw new IllegalArgumentException("Null or empty attribute name passed");
        }

        Map<String, Object> map = new LinkedHashMap<>();
        map.put(GROUP, group);
        map.put(MEMBER, member);
        map.put(ATTRIBUTE_NAME, attributeName);

        JsonNode response = connectorRpc.post(ATTRIBUTES_MANAGER, GET_ATTRIBUTE, map);

        return PerunRpcMapper.mapAttributeValue(response);
    }

    private Map<String, PerunAttributeValue> getUserExtSourceAttributeValues(Long uesId,
                                                                             List<String> attributeNames)
    {
        if (uesId == null) {
            throw new IllegalArgumentException("Null user ext source ID passed");
        } else if (attributeNames == null || attributeNames.isEmpty()) {
            throw new IllegalArgumentException("Null or empty list of attribute names passed");
        }

        Map<String, Object> map = new LinkedHashMap<>();

        map.put(USER_EXT_SOURCE, uesId);
        map.put(ATTR_NAMES, attributeNames);

        JsonNode response = connectorRpc.post(ATTRIBUTES_MANAGER, GET_ATTRIBUTES, map);

        return PerunRpcMapper.mapAttributes(response);
    }

    private void addAffiliationsFromGroupsForMember(@NonNull String groupAffiliationsAttr,
                                                    @NonNull List<Affiliation> affiliations,
                                                    @NonNull Member member)
    {
        List<Group> memberGroups = getMemberGroups(member.getId());
        for (Group group : memberGroups) {
            PerunAttributeValue attrValue = getGroupAttributeValue(group, groupAffiliationsAttr);
            if (attrValue != null && attrValue.valueAsList() != null) {
                long linuxTime = System.currentTimeMillis() / 1000L;

                for (String value : attrValue.valueAsList()) {
                    Affiliation affiliation = new Affiliation(null, value, linuxTime);
                    log.debug("found {} on group {}", value, group.getName());
                    affiliations.add(affiliation);
                }
            }
        }
    }

}
