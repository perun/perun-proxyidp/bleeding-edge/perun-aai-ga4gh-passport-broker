package cz.muni.ics.ga4gh.persistence.database.properties;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@NoArgsConstructor
@Slf4j
@Valid
public class DatabaseProperties {

    @NotBlank private String driver;

    @NotBlank private String url;

    @NotBlank private String username;

    @NotBlank private String password;

    private int maxPoolSize = 40;

    @PostConstruct
    public void postInit() {
        log.info("Initialized JDBC properties");
        log.debug("{}", this);
    }

    @Override
    public String toString() {
        return "JdbcProperties{" +
                "driver='" + driver + '\'' +
                ", url='" + url + '\'' +
                ", username='" + username + '\'' +
                ", password='*******************'" +
                '}';
    }

}
