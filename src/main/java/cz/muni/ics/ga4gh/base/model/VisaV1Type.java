package cz.muni.ics.ga4gh.base.model;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import org.springframework.util.StringUtils;

@Getter
@AllArgsConstructor
public enum VisaV1Type {

    ACCEPTED_TERMS_AND_POLICIES("AcceptedTermsAndPolicies"),
    AFFILIATION_AND_ROLE("AffiliationAndRole"),
    CONTROLLED_ACCESS_GRANTS("ControlledAccessGrants"),
    LINKED_IDENTITIES("LinkedIdentities"),
    RESEARCHER_STATUS("ResearcherStatus");

    private final String value;

    public static VisaV1Type lookup(@NonNull String value) {
        if (!StringUtils.hasText(value)) {
            throw new IllegalArgumentException("arg 'value' cannot be blank string");
        }
        VisaV1Type result = lookup.getOrDefault(value, null);
        if (result == null) {
            throw new IllegalArgumentException("unrecognized value");
        }
        return result;
    }

    @Override
    public String toString() {
        return value;
    }

    private static final Map<String, VisaV1Type> lookup;

    static {
        Map<String, VisaV1Type> map = new HashMap<>();
        for (VisaV1Type t : VisaV1Type.values()) {
            map.put(t.value, t);
        }
        lookup = Collections.unmodifiableMap(map);
    }


}
