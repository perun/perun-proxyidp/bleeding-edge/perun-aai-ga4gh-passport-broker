package cz.muni.ics.ga4gh.base.credentials;

import cz.muni.ics.ga4gh.base.exceptions.ConfigurationException;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;

@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@Validated
public class BasicAuthCredentials implements InitializingBean {
    @NotBlank
    private String username;
    @NotBlank
    private String password;

    @Override
    public void afterPropertiesSet() throws Exception {
        if (!StringUtils.hasText(username) || !StringUtils.hasText(password)) {
            throw new ConfigurationException(
                "Invalid basic-auth credentials configured - empty username or password.");
        }
    }

    @Override
    public String toString() {
        return "BasicAuthCredentials{" +
            "username='" + username + '\'' +
            ", password='PROTECTED_STRING'" +
            '}';
    }
}