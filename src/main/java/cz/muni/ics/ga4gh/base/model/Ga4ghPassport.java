package cz.muni.ics.ga4gh.base.model;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.nimbusds.jose.JOSEObjectType;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import cz.muni.ics.ga4gh.base.Utils;
import cz.muni.ics.ga4gh.middleware.service.JWTSigningService;
import java.net.URI;
import java.time.LocalDateTime;
import java.util.List;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Ga4ghPassport {

    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public static final String SUB = "sub";
    public static final String ISS = "iss";
    public static final String IAT = "iat";
    public static final String EXP = "exp";
    public static final String JTI = "jti";

    // === PASSPORT HEADER FIELDS ===
    private String kid;

    private final JOSEObjectType typ = new JOSEObjectType("vnd.ga4gh.passport+jwt");

    private URI jku;

    // === PASSPORT PAYLOAD FIELDS ===

    private String iss;

    private String sub;

    private LocalDateTime iat;

    private LocalDateTime exp;

    private String jti;

    private List<String> aud;

    // value of the visa
    private Ga4ghPassportClaim ga4ghPassportV1;

    // === CUSTOM FIELDS FOR WORKING WITH PASSPORTS ===

    @ToString.Exclude
    private String signer = null;

    private boolean verified = false;
    private SignedJWT jwt;

    public void generateSignedJwt(JWTSigningService jwtService) {
        JWTClaimsSet jwtClaimsSet = new JWTClaimsSet.Builder()
            .issuer(iss)
            .issueTime(Utils.toDate(iat))
            .expirationTime(Utils.toDate(exp))
            .subject(sub)
            .jwtID(jti)
            .claim(Ga4ghPassportClaim.GA4GH_PASSPORT_V1, ga4ghPassportV1.serialize())
            .build();

        JWSAlgorithm algorithm = JWSAlgorithm.parse(jwtService.getSigningAlgorithm().getName());
        JWSHeader jwsHeader = new JWSHeader.Builder(algorithm)
            .keyID(jwtService.getSignerKeyId())
            .type(typ)
            .jwkURL(jku)
            .build();

        SignedJWT signedTokenJWT = new SignedJWT(jwsHeader, jwtClaimsSet);
        jwtService.signJwt(signedTokenJWT);
        this.jwt = signedTokenJWT;
    }

    public String serialize() {
        return jwt.serialize();
    }

    public JsonNode asJson() {
        String jsonString = serialize();
        return JsonNodeFactory.instance.textNode(jsonString);
    }

}
