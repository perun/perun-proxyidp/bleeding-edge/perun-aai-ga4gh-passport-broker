package cz.muni.ics.ga4gh.base;

import static cz.muni.ics.ga4gh.base.model.Ga4ghPassportVisa.ASSERTED;
import static cz.muni.ics.ga4gh.base.model.Ga4ghPassportVisa.BY;
import static cz.muni.ics.ga4gh.base.model.Ga4ghPassportVisa.CONDITIONS;
import static cz.muni.ics.ga4gh.base.model.Ga4ghPassportVisa.SOURCE;
import static cz.muni.ics.ga4gh.base.model.Ga4ghPassportVisa.TYPE;
import static cz.muni.ics.ga4gh.base.model.Ga4ghPassportVisa.VALUE;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.JWTParser;
import com.nimbusds.jwt.SignedJWT;
import cz.muni.ics.ga4gh.base.model.Ga4ghPassportVisa;
import cz.muni.ics.ga4gh.base.model.Ga4ghPassportVisaClaim;
import cz.muni.ics.ga4gh.base.model.VisaV1Type;
import cz.muni.ics.ga4gh.middleware.service.JWSValidationService;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.yaml.snakeyaml.util.UriEncoder;

@Slf4j
public class Utils {

    public static Ga4ghPassportVisa parseVisa(String jwtString) {
        try {
            SignedJWT signedJWT = (SignedJWT) JWTParser.parse(jwtString);
            return parseVisa(signedJWT);
        } catch (Exception ex) {
            log.error("Visa '{}' cannot be parsed", jwtString, ex);
        }
        return null;
    }

    public static Ga4ghPassportVisa parseVisa(SignedJWT jwt)
        throws ParseException, JsonProcessingException {
        JWSHeader header = jwt.getHeader();
        JWTClaimsSet payloadClaimSet = jwt.getJWTClaimsSet();

        Ga4ghPassportVisa visa = new Ga4ghPassportVisa();

        JsonNode visaPayload =
            new ObjectMapper().readValue(jwt.getPayload().toString(), JsonNode.class);
        JsonNode visaV1 = visaPayload.path(Ga4ghPassportVisaClaim.GA4GH_VISA_V1);
        boolean isCorrect = checkVisaHeader(header) && checkVisaValue(visaV1);

        if (!isCorrect) {
            log.debug("Visa '{}' (payload '{}') did not pass fields validation", jwt, visaPayload);
            return visa;
        }

        visa.setKid(header.getKeyID());
        visa.setJku(header.getJWKURL());
        visa.setTyp(header.getType());

        visa.setSub(payloadClaimSet.getSubject());
        visa.setIss(payloadClaimSet.getIssuer());
        if (payloadClaimSet.getIssueTime() != null) {
            visa.setIat(toLocalDateTime(payloadClaimSet.getIssueTime()));
        } else {
            visa.setIat(null);
        }
        if (payloadClaimSet.getExpirationTime() != null) {
            visa.setExp(toLocalDateTime(payloadClaimSet.getExpirationTime()));
        } else {
            visa.setExp(null);
        }
        visa.setJti(payloadClaimSet.getJWTID());
        visa.setGa4ghVisaV1(parseVisaV1(visaV1));
        visa.setLinkedIdentity(Utils.constructLinkedIdentity(visa.getSub(), visa.getIss()));
        visa.setJwt(jwt);

        return visa;
    }

    private static boolean checkVisaExpiration(Ga4ghPassportVisa visa) {
        LocalDateTime exp = visa.getExp();
        LocalDateTime now = LocalDateTime.now();
        boolean valid = exp != null && now.isBefore(exp);
        if (!valid) {
            log.warn("Visa did not pass expiration validation. Visa expired on '{}'.",
                exp);
        }
        return valid;
    }

    public static Ga4ghPassportVisaClaim parseVisaV1(JsonNode visaV1Json) {
        Ga4ghPassportVisaClaim visaV1 = new Ga4ghPassportVisaClaim();
        visaV1.setAsserted(visaV1Json.path(ASSERTED).longValue());
        visaV1.setSource(visaV1Json.path(SOURCE).textValue());
        visaV1.setType(VisaV1Type.lookup(visaV1Json.path(TYPE).textValue()));
        visaV1.setValue(visaV1Json.path(VALUE).textValue());
        if (visaV1Json.hasNonNull(BY)) {
            visaV1.setBy(visaV1Json.path(BY).textValue());
        }
        if (visaV1Json.hasNonNull(CONDITIONS)) {
            visaV1.setConditions(visaV1Json.get(CONDITIONS));
        }
        return visaV1;
    }

    private static boolean checkVisaHeader(JWSHeader visaHeader) {
        boolean valid = visaHeader.getJWKURL() != null
            && visaHeader.getType() != null
            && StringUtils.hasText(visaHeader.getKeyID());
        if (!valid) {
            log.debug("Visa header did not pass verification");
        }
        return valid;
    }

    private static boolean checkVisaValue(JsonNode visaV1) {
        if (visaV1.isMissingNode() || visaV1.isNull() || visaV1.isEmpty()) {
            log.warn("Visa value ({}) is not present. Visa did not pass value verification.",
                Ga4ghPassportVisaClaim.GA4GH_VISA_V1);
            return false;
        }
        boolean keysValid = checkKeyPresence(visaV1, TYPE)
            && checkKeyPresence(visaV1, Ga4ghPassportVisa.ASSERTED)
            && checkKeyPresence(visaV1, Ga4ghPassportVisa.VALUE)
            && checkKeyPresence(visaV1, Ga4ghPassportVisa.SOURCE);
        if (!keysValid) {
            log.debug("Visa value did not pass verification of required keys presence.");
        }
        return keysValid;
    }

    private static boolean checkKeyPresence(JsonNode jsonNode, String key) {
        if (jsonNode.path(key).isMissingNode()) {
            log.warn("Key '{}' is missing in the Visa.", key);
            return false;
        }
        return true;
    }

    public static void verifyVisa(@NonNull Ga4ghPassportVisa visa,
                                  @NonNull JWSValidationService validationService) {
        SignedJWT jwt = visa.getJwt();
        try {
            boolean expirationVerified = checkVisaExpiration(visa);
            if (!expirationVerified) {
                log.warn("Visa is expired, Visa verification failed");
                visa.setVerified(false);
                return;
            }

            if (!validationService.validateJwt(jwt)) {
                log.warn("Visa signature verification failed, Visa verification failed");
                visa.setVerified(false);
                return;
            }
            visa.setVerified(true);
        } catch (Exception ex) {
            log.error("Visa '{}' cannot be verified", jwt, ex);
        }
    }

    public static LocalDateTime toLocalDateTime(long epochSeconds) {
        return LocalDateTime.ofInstant(Instant.ofEpochSecond(epochSeconds),ZoneOffset.UTC);
    }

    public static LocalDateTime toLocalDateTime(@NonNull Date dateToConvert) {
        return dateToConvert.toInstant()
            .atZone(ZoneId.systemDefault())
            .toLocalDateTime();
    }

    public static Date toDate(@NonNull LocalDateTime dateToConvert) {
        return Date.from(
            dateToConvert.atZone(ZoneId.systemDefault()).toInstant()
        );
    }

    public static long getOneYearExpires(long asserted) {
        return getExpires(asserted, 1L);
    }

    public static long getExpires(long asserted, long addYears) {
        return Instant.ofEpochSecond(asserted).atZone(ZoneId.systemDefault()).plusYears(addYears).toEpochSecond();
    }

    public static String constructLinkedIdentity(String sub, String iss) {
        return UriEncoder.encode(sub) + ',' + UriEncoder.encode(iss);
    }

}
