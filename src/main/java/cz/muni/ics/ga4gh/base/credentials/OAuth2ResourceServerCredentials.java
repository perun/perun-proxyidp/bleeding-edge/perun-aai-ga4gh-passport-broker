package cz.muni.ics.ga4gh.base.credentials;

import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import org.hibernate.validator.constraints.URL;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.validation.annotation.Validated;

@Getter
@ToString
@AllArgsConstructor
@Validated
@ConstructorBinding
public class OAuth2ResourceServerCredentials {

    @NotBlank
    @URL
    private String introspectionUri;

    @NotBlank
    private String introspectionClientId;

    @ToString.Exclude
    @NotBlank
    private String introspectionClientSecret;

}
