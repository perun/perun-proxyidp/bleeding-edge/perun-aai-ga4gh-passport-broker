package cz.muni.ics.ga4gh.base.properties;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.validation.annotation.Validated;

@Getter
@ToString
@Slf4j

@Validated
@ConstructorBinding
public class PerunBrokerInstanceProperties {

    @NotBlank
    private final String name;

    @NotBlank
    private final String instanceClass;

    @NotBlank
    private final String identifierAttribute;

    @NotBlank
    private final String visaIssuer;

    private final Long membershipVoId;

    // RESEARCHER STATUS THINGS
    private final Long termsAndPoliciesGroupId;

    private final String bonaFideStatusAttr;

    private final String bonaFideStatusRemsAttr;

    // MANUAL AFFILIATIONS
    private final String groupAffiliationsAttr;

    private final String groupAffiliationsOrgUrlAttr;

    private final Long groupAffiliationsRootGroupId;

    private final Long groupAffiliationsVoId;

    private final String groupAffiliationsGroupMemberExpirationAttr;

    // UES AFFILIATIONS

    private final String uesAffiliationsAttr;

    private final Integer passportRepositoriesTimeout;

    private final List<Ga4ghClaimRepositoryProperties> passportRepositories = new ArrayList<>();

    public PerunBrokerInstanceProperties(String name,
                                         String instanceClass,
                                         String identifierAttribute,
                                         String visaIssuer,
                                         Long membershipVoId,
                                         Long termsAndPoliciesGroupId,
                                         String bonaFideStatusAttr,
                                         String bonaFideStatusRemsAttr,
                                         String uesAffiliationsAttr,
                                         String groupAffiliationsAttr,
                                         String groupAffiliationsOrgUrlAttr,
                                         Long groupAffiliationsRootGroupId,
                                         Long groupAffiliationsVoId,
                                         String groupAffiliationsGroupMemberExpirationAttr,
                                         List<Ga4ghClaimRepositoryProperties> passportRepositories,
                                         Integer passportRepositoriesTimeout)
    {
        this.name = name;
        this.instanceClass = instanceClass;
        this.identifierAttribute = identifierAttribute;
        this.membershipVoId = membershipVoId;
        this.bonaFideStatusAttr = bonaFideStatusAttr;
        this.bonaFideStatusRemsAttr = bonaFideStatusRemsAttr;
        this.termsAndPoliciesGroupId = termsAndPoliciesGroupId;
        this.uesAffiliationsAttr = uesAffiliationsAttr;
        this.groupAffiliationsAttr = groupAffiliationsAttr;
        this.groupAffiliationsOrgUrlAttr = groupAffiliationsOrgUrlAttr;
        this.groupAffiliationsRootGroupId = groupAffiliationsRootGroupId;
        this.groupAffiliationsVoId = groupAffiliationsVoId;
        this.groupAffiliationsGroupMemberExpirationAttr =
            groupAffiliationsGroupMemberExpirationAttr;
        this.visaIssuer = visaIssuer;
      this.passportRepositoriesTimeout = passportRepositoriesTimeout;
      if (passportRepositories != null) {
            this.passportRepositories.addAll(passportRepositories);
        }
    }

    @PostConstruct
    public void init() {
        log.info("Initialized '{}' properties", this.getClass().getSimpleName());
        log.debug("{}", this);
    }

}
