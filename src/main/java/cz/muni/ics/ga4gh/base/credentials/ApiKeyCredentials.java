package cz.muni.ics.ga4gh.base.credentials;

import cz.muni.ics.ga4gh.base.exceptions.ConfigurationException;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;

@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@Validated
public class ApiKeyCredentials implements InitializingBean {
    @NotBlank
    private String apiKeyHeader;
    @NotBlank
    private String apiKeyValue;

    @Override
    public void afterPropertiesSet() throws Exception {
        if (!StringUtils.hasText(apiKeyHeader) || !StringUtils.hasText(apiKeyValue)) {
            throw new ConfigurationException(
                "Invalid api-key credentials configured - empty header or value.");
        }
    }

    @Override
    public String toString() {
        return "ApiKeyCredentials{" +
            "apiKeyHeader='" + apiKeyHeader + '\'' +
            ", apiKeyValue='PROTECTED_STRING'" +
            '}';
    }
}