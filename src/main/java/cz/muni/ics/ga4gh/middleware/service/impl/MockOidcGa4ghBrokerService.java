package cz.muni.ics.ga4gh.middleware.service.impl;

import cz.muni.ics.ga4gh.base.model.Ga4ghPassport;
import cz.muni.ics.ga4gh.base.model.Ga4ghPassportClaim;
import cz.muni.ics.ga4gh.base.model.Ga4ghPassportVisa;
import cz.muni.ics.ga4gh.base.model.Ga4ghTokenAuthorization;
import cz.muni.ics.ga4gh.base.properties.MockBrokerProperties;
import cz.muni.ics.ga4gh.middleware.broker.MockOidcGa4ghBroker;
import cz.muni.ics.ga4gh.middleware.service.Ga4ghBrokerService;
import cz.muni.ics.ga4gh.middleware.service.JWTSigningService;
import cz.muni.ics.ga4gh.middleware.service.ServiceUtils;
import java.util.List;
import lombok.NonNull;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnBean({MockOidcGa4ghBroker.class, MockBrokerProperties.class})
public class MockOidcGa4ghBrokerService implements Ga4ghBrokerService {

    private final MockOidcGa4ghBroker broker;

    private final JWTSigningService signingService;

    private final MockBrokerProperties mockBrokerProperties;

    public MockOidcGa4ghBrokerService(@NonNull MockOidcGa4ghBroker broker,
                                      @NonNull JWTSigningService signingService,
                                      @NonNull MockBrokerProperties mockBrokerProperties)
    {
        this.broker = broker;
        this.signingService = signingService;
        this.mockBrokerProperties = mockBrokerProperties;
    }

    @Override
    public Ga4ghPassportClaim getGa4ghPassportUserinfoStyle(@NonNull String userId) {
        Ga4ghPassportClaim passport = new Ga4ghPassportClaim();
        List<Ga4ghPassportVisa> visas = broker.constructGa4ghPassportVisas(userId);
        if (visas != null) {
            passport.addVisas(visas);
        }
        return passport;
    }

    @Override
    public Ga4ghPassport getGa4ghPassportTokenStyle(@NonNull Ga4ghTokenAuthorization accessToken) {
        Ga4ghPassportClaim passportClaim = getGa4ghPassportUserinfoStyle(accessToken.getSub());
        return ServiceUtils.getGa4ghPassport(accessToken, passportClaim, mockBrokerProperties.getJku(),
            mockBrokerProperties.getIssuer(), signingService);
    }

}
