package cz.muni.ics.ga4gh.middleware.broker.helper;

import com.fasterxml.jackson.databind.JsonNode;
import cz.muni.ics.ga4gh.base.model.VisaV1Type;
import cz.muni.ics.ga4gh.middleware.service.JWTSigningService;
import java.net.URI;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class VisaAssemblyParameters {

    private String issuer;

    private URI jku;

    private VisaV1Type type;

    private String sub;

    private Long userId;

    private String value;

    private String source;

    private String by;

    private long asserted;

    private long expires;

    private JsonNode conditions;

    private String signer;

    private JWTSigningService jwtService;

}
