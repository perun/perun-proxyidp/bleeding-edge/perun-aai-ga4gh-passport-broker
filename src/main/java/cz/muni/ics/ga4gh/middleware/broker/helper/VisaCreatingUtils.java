package cz.muni.ics.ga4gh.middleware.broker.helper;

import static cz.muni.ics.ga4gh.base.Utils.toLocalDateTime;

import com.nimbusds.jose.JOSEObjectType;
import cz.muni.ics.ga4gh.base.model.Ga4ghPassportVisa;
import cz.muni.ics.ga4gh.base.model.Ga4ghPassportVisaClaim;
import cz.muni.ics.ga4gh.middleware.service.JWTSigningService;
import java.net.URI;
import java.time.LocalDateTime;
import java.util.UUID;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class VisaCreatingUtils {

    public static Ga4ghPassportVisa createVisa(@NonNull VisaAssemblyParameters parameters) {
        long now = System.currentTimeMillis() / 1000L;

        if (parameters.getAsserted() > now) {
            log.warn("Visa asserted in future, it will be ignored!");
            log.debug("Visa information: {}", parameters);
            return null;
        }

        if (parameters.getExpires() <= now) {
            log.warn("Visa is expired, it will be ignored!");
            log.debug("Visa information: {}", parameters);
            return null;
        }

        Ga4ghPassportVisaClaim ga4ghVisaV1 = new Ga4ghPassportVisaClaim();
        ga4ghVisaV1.setAsserted(parameters.getAsserted());
        ga4ghVisaV1.setBy(parameters.getBy());
        ga4ghVisaV1.setValue(parameters.getValue());
        ga4ghVisaV1.setType(parameters.getType());
        ga4ghVisaV1.setSource(parameters.getSource());
        ga4ghVisaV1.setConditions(parameters.getConditions());

        Ga4ghPassportVisa visa = new Ga4ghPassportVisa();
        visa.setKid(parameters.getJwtService().getSignerKeyId());
        visa.setTyp(JOSEObjectType.JWT);
        visa.setJku(parameters.getJku());

        visa.setIss(parameters.getIssuer());
        visa.setIat(LocalDateTime.now());
        visa.setExp(toLocalDateTime(parameters.getExpires()));
        visa.setSub(parameters.getSub());
        visa.setJti(UUID.randomUUID().toString());
        visa.setGa4ghVisaV1(ga4ghVisaV1);

        visa.setVerified(true);
        visa.generateSignedJwt(parameters.getJwtService());

        return visa;
    }

    public static Ga4ghPassportVisa createVisa(@NonNull String visaIssuer,
                                               @NonNull URI visaIssuerJku,
                                               @NonNull JWTSigningService signingService,
                                               @NonNull VisaAssemblyParameters parameters)
    {
        parameters.setIssuer(visaIssuer);
        parameters.setJku(visaIssuerJku);
        parameters.setSigner(visaIssuer);
        parameters.setJwtService(signingService);
        return createVisa(parameters);
    }

}
