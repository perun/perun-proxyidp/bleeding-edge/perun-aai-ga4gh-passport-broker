package cz.muni.ics.ga4gh.middleware.broker;

import cz.muni.ics.ga4gh.base.model.Ga4ghPassportVisa;
import java.util.List;
import lombok.NonNull;

public interface PerunBasedGa4ghBroker {

    List<Ga4ghPassportVisa> constructGa4ghPassportVisas(@NonNull Long userId);

    String getName();

}
