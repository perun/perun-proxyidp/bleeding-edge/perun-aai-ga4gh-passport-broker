package cz.muni.ics.ga4gh.middleware.broker.helper;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.ics.ga4gh.base.Utils;
import cz.muni.ics.ga4gh.base.model.ClaimRepositoryHeader;
import cz.muni.ics.ga4gh.base.model.Ga4ghClaimRepository;
import cz.muni.ics.ga4gh.base.model.Ga4ghPassportVisa;
import cz.muni.ics.ga4gh.base.properties.Ga4ghClaimRepositoryProperties;
import cz.muni.ics.ga4gh.middleware.service.JWSValidationService;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.MediaType;
import org.springframework.http.client.InterceptingClientHttpRequestFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Slf4j
public class ExternalRepositoryUtils {

    public static final String REPOSITORY_URL_USER_ID = "user_id";


    public static final String GA4GH_CLAIM = "ga4gh_passport_v1";

    public static final String JSON = "json";

    public static void callExternalRepositories(
        @NonNull String sub,
        @NonNull List<Ga4ghClaimRepository> claimRepositories,
        @NonNull List<Ga4ghPassportVisa> controlledAccessGrantsFromRepositories,
        @NonNull Set<String> linkedIdentitiesFromRepositories,
        @NonNull JWSValidationService validationService)
    {
        for (Ga4ghClaimRepository repository: claimRepositories) {
            callPermissionsJwtAPI(
                repository,
                Collections.singletonMap(REPOSITORY_URL_USER_ID, sub),
                controlledAccessGrantsFromRepositories,
                linkedIdentitiesFromRepositories,
                validationService
            );
        }

    }

    protected static void callPermissionsJwtAPI(Ga4ghClaimRepository repo,
                                                Map<String, String> uriVariables,
                                                List<Ga4ghPassportVisa> controlledAccessGrantsList,
                                                Set<String> linkedIdentitiesSet,
                                                JWSValidationService validationService)
    {
        log.debug("Calling claim repository '{}' with parameters '{}'", repo, uriVariables);
        JsonNode response = callHttpJsonAPI(repo, uriVariables);
        if (response == null) {
            log.debug("No response returned");
            return;
        } else if (!response.hasNonNull(GA4GH_CLAIM)) {
            log.debug("Response does not contain non null value for key '{}'", GA4GH_CLAIM);
            return;
        }

        JsonNode visas = response.path(GA4GH_CLAIM);
        if (!visas.isArray()) {
            log.warn("'{}' claim is not an array. Received response '{}'", GA4GH_CLAIM, response);
        }
        for (JsonNode visaNode : visas) {
            if (!visaNode.isTextual()) {
                log.warn("Element '{}' of '{}' is not a String, skipping value", visaNode, GA4GH_CLAIM);
                continue;
            }
            Ga4ghPassportVisa visa = Utils.parseVisa(visaNode.asText());
            if (visa == null) {
                log.debug("Visa '{}' could not be parsed", visaNode);
                continue;
            }
            Utils.verifyVisa(visa, validationService);
            if (visa.isVerified()) {
                log.debug("Adding a visa to passport: {}", visa);
                controlledAccessGrantsList.add(visa);
                linkedIdentitiesSet.add(visa.getLinkedIdentity());
            } else {
                log.warn("Skipping visa: {}", visa);
            }
        }
    }

    protected static JsonNode callHttpJsonAPI(Ga4ghClaimRepository repo,
                                              Map<String, String> uriVariables)
    {
        //get permissions data
        try {
            JsonNode result;
            try {
                if (log.isDebugEnabled()) {
                    log.debug("Calling Permissions API at {}", repo.getRestTemplate()
                        .getUriTemplateHandler().expand(repo.getActionURL(), uriVariables));
                }

                result = repo.getRestTemplate().getForObject(repo.getActionURL(), JsonNode.class, uriVariables);
            } catch (HttpClientErrorException ex) {
                MediaType contentType = null;
                if (ex.getResponseHeaders() != null) {
                    contentType = ex.getResponseHeaders().getContentType();
                }
                String body = ex.getResponseBodyAsString();

                log.error("HTTP ERROR: {}, URL: {}, Content-Type: {}",
                    ex.getRawStatusCode(), repo.getActionURL(), contentType);

                if (ex.getRawStatusCode() == 404) {
                    log.warn("Got status 404 from Permissions endpoint {}, user is not linked to user at Permissions API",
                        repo.getActionURL());
                    return null;
                }

                if (contentType != null && JSON.equals(contentType.getSubtype())) {
                    try {
                        log.error(new ObjectMapper().readValue(body, JsonNode.class).path("message").asText());
                    } catch (IOException e) {
                        log.error("cannot parse error message from JSON", e);
                    }
                } else {
                    log.error("cannot make REST call, exception: {} message: {}", ex.getClass().getName(), ex.getMessage());
                }
                return null;
            }
            log.debug("Permissions API response: {}", result);
            return result;
        } catch (Exception ex) {
            log.error("Cannot get dataset permissions", ex);
        }
        return null;
    }

    public static void initializeClaimRepository(
        @NonNull Ga4ghClaimRepositoryProperties ga4ghClaimRepositoryProperties,
        @NonNull List<Ga4ghClaimRepository> claimRepositories,
        @NonNull Integer connectTimeout
        )
    {
        String name = ga4ghClaimRepositoryProperties.getName();
        String actionURL = ga4ghClaimRepositoryProperties.getUrl();
        List<ClaimRepositoryHeader> headers = ga4ghClaimRepositoryProperties.getHeaders();

        if (actionURL == null || headers.isEmpty()) {
            log.error("claim repository '{}' not defined with url|auth_header|auth_value",
                ga4ghClaimRepositoryProperties);
            return;
        }
        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        restTemplateBuilder.setConnectTimeout(Duration.ofSeconds(connectTimeout));
        RestTemplate restTemplate = restTemplateBuilder.build();
        restTemplate.setRequestFactory(
            new InterceptingClientHttpRequestFactory(
                restTemplate.getRequestFactory(), new ArrayList<>(headers)
            )
        );

        claimRepositories.add(new Ga4ghClaimRepository(name, actionURL, restTemplate));
        log.info("GA4GH Claims Repository '{}' configured at '{}'", name, actionURL);
    }

    public static void initializeRepositoryValidationKeys(@NonNull String name,
                                                          @NonNull String jwks,
                                                          @NonNull JWSValidationService validationService)
    {
        try {
            URL jku = new URL(jwks);
            validationService.loadRemoteJwkSet(jku);
            log.info("JWKS Signer '{}' added with keys '{}'", name, jwks);
        } catch (MalformedURLException ex) {
            log.error("cannot add to RemoteJWKSet map: '{}' -> '{}'", name, jwks, ex);
        }
    }

}
