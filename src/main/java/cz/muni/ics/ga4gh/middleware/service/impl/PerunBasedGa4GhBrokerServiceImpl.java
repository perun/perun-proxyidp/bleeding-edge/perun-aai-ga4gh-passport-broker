package cz.muni.ics.ga4gh.middleware.service.impl;

import cz.muni.ics.ga4gh.base.exceptions.ConfigurationException;
import cz.muni.ics.ga4gh.base.model.Ga4ghPassport;
import cz.muni.ics.ga4gh.base.model.Ga4ghPassportClaim;
import cz.muni.ics.ga4gh.base.model.Ga4ghPassportVisa;
import cz.muni.ics.ga4gh.base.model.Ga4ghTokenAuthorization;
import cz.muni.ics.ga4gh.base.properties.BrokersProperties;
import cz.muni.ics.ga4gh.base.properties.PerunBrokerInstanceProperties;
import cz.muni.ics.ga4gh.middleware.broker.PerunBasedGa4ghBroker;
import cz.muni.ics.ga4gh.middleware.broker.impl.AbstractPerunBasedGa4ghBroker;
import cz.muni.ics.ga4gh.middleware.service.Ga4ghBrokerService;
import cz.muni.ics.ga4gh.middleware.service.JWSValidationService;
import cz.muni.ics.ga4gh.middleware.service.JWTSigningService;
import cz.muni.ics.ga4gh.middleware.service.ServiceUtils;
import cz.muni.ics.ga4gh.middleware.service.exception.UserNotFoundException;
import cz.muni.ics.ga4gh.middleware.service.exception.UserNotUniqueException;
import cz.muni.ics.ga4gh.persistence.database.repository.VisaEntityRepository;
import cz.muni.ics.ga4gh.persistence.perun.adapters.PerunAdapterRpc;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
@Slf4j
@ConditionalOnBean({PerunAdapterRpc.class, BrokersProperties.class})
public class PerunBasedGa4GhBrokerServiceImpl implements Ga4ghBrokerService {

    private final PerunAdapterRpc adapter;

    private final List<String> userIdentificationAttributes = new ArrayList<>();

    private final JWTSigningService signingService;

    private final JWSValidationService validationService;

    private final VisaEntityRepository visaEntityRepository;

    private final BrokersProperties properties;

    private final List<PerunBasedGa4ghBroker> brokers;

    @Autowired
    public PerunBasedGa4GhBrokerServiceImpl(@NonNull PerunAdapterRpc adapter,
                                            @NonNull BrokersProperties brokersProperties,
                                            @NonNull JWTSigningService jwtSigningService,
                                            @NonNull JWSValidationService jwsValidationService,
                                            Optional<VisaEntityRepository> visaEntityRepository)
        throws ConfigurationException
    {
        this.properties = brokersProperties;
        this.adapter = adapter;
        this.signingService = jwtSigningService;
        this.validationService = jwsValidationService;
        List<PerunBasedGa4ghBroker> brokers = new ArrayList<>();
        for (PerunBrokerInstanceProperties bip: brokersProperties.getInstances()) {
            brokers.add(initializeBroker(bip));
        }
        this.brokers = Collections.unmodifiableList(brokers);
        this.userIdentificationAttributes.addAll(brokersProperties.getUserIdentificationAttributes());
        this.visaEntityRepository = visaEntityRepository.orElse(null);
    }

    private PerunBasedGa4ghBroker initializeBroker(PerunBrokerInstanceProperties bip)
        throws ConfigurationException
    {
        String brokerName = bip.getName();
        String sourceClass = bip.getInstanceClass();
        if (!StringUtils.hasText(sourceClass)) {
            log.error("{} - failed to initialized broker instance: no class has ben configured",
                brokerName);
            throw new ConfigurationException("No class configured source");
        }

        log.trace("{} - loading Ga4ghBroker class '{}'", brokerName, sourceClass);

        try {
            Class<?> rawClazz = Class.forName(sourceClass);
            if (!PerunBasedGa4ghBroker.class.isAssignableFrom(rawClazz)) {
                log.error("{} - failed to initialized Ga4ghBroker: class '{}' does not implement Ga4ghBroker interface",
                    brokerName, sourceClass);
                throw new ConfigurationException("No instantiable class source configured " + brokerName);
            }
            if (AbstractPerunBasedGa4ghBroker.class.isAssignableFrom(rawClazz)) {
                @SuppressWarnings("unchecked") Class<AbstractPerunBasedGa4ghBroker> clazz =
                    (Class<AbstractPerunBasedGa4ghBroker>) rawClazz;
                Constructor<AbstractPerunBasedGa4ghBroker> constructor =
                    clazz.getConstructor(
                        PerunBrokerInstanceProperties.class,
                        PerunAdapterRpc.class,
                        URI.class,
                        JWTSigningService.class,
                        JWSValidationService.class,
                        VisaEntityRepository.class
                    );
                return constructor.newInstance(
                    bip, adapter, properties.getJku(), signingService,
                    validationService, visaEntityRepository
                );
            }
            throw new UnsupportedOperationException("No way to instantiate implemented " + sourceClass);
        } catch (ClassNotFoundException e) {
            log.error("{} - failed to initialize Ga4ghBroker: class '{}' was not found", brokerName, sourceClass);
            log.trace("{} - details:", brokerName, e);
            throw new ConfigurationException("Error has occurred when instantiating Ga4ghBroker " + brokerName);
        } catch (NoSuchMethodException e) {
            log.error("{} - failed to initialize Ga4ghBroker: class '{}' does not have proper constructor",
                brokerName, sourceClass);
            log.trace("{} - details:", brokerName, e);
            throw new ConfigurationException("Error has occurred when instantiating Ga4ghBroker " + brokerName);
        } catch (IllegalAccessException | InvocationTargetException | InstantiationException e) {
            log.error("{} - failed to initialize Ga4ghBroker: class '{}' cannot be instantiated", brokerName, sourceClass);
            log.trace("{} - details:", brokerName, e);
            throw new ConfigurationException("Error has occurred when instantiating Ga4ghBroker " + brokerName);
        }
    }

    @Override
    public Ga4ghPassportClaim getGa4ghPassportUserinfoStyle(@NonNull String userId)
        throws UserNotFoundException, UserNotUniqueException
    {
        Long perunUserId = identifyUser(userId);
        if (perunUserId == null) {
            throw new UserNotFoundException("No user found for given identifier");
        }
        return getGa4ghPassportUserinfoStyle(perunUserId);
    }

    @Override
    public Ga4ghPassport getGa4ghPassportTokenStyle(@NonNull Ga4ghTokenAuthorization accessToken)
        throws UserNotFoundException, UserNotUniqueException
    {
        Long perunUserId = identifyUser(accessToken.getSub());
        if (perunUserId == null) {
            throw new UserNotFoundException("No user found for given identifier");
        }
        return getGa4ghPassportTokenStyle(perunUserId, accessToken);
    }

    private Ga4ghPassportClaim getGa4ghPassportUserinfoStyle(@NonNull Long userId) {
        Ga4ghPassportClaim passport = new Ga4ghPassportClaim();

        for (PerunBasedGa4ghBroker broker: brokers) {
            long localStartTime = System.currentTimeMillis();
            List<Ga4ghPassportVisa> visas = broker.constructGa4ghPassportVisas(userId);
            if (visas != null) {
                passport.addVisas(visas);
            }
            long localEndTime = System.currentTimeMillis();
            log.info("Generating for user '{}' in broker '{}' took {}ms",
                userId, broker.getName(), localEndTime - localStartTime);
        }
        return passport;
    }

    private Ga4ghPassport getGa4ghPassportTokenStyle(@NonNull Long userId,
                                                     @NonNull Ga4ghTokenAuthorization accessToken)
    {
        Ga4ghPassportClaim passportClaim = getGa4ghPassportUserinfoStyle(userId);
        return ServiceUtils.getGa4ghPassport(accessToken, passportClaim, properties.getJku(),
            properties.getIssuer(),
            signingService);
    }

    private Long identifyUser(@NonNull String userIdentifier)
        throws UserNotFoundException, UserNotUniqueException
    {
        if (!StringUtils.hasText(userIdentifier)) {
            throw new IllegalArgumentException("User identifier cannot be empty");
        }

        Set<Long> perunUserIds = new HashSet<>();

        for (String attrName : userIdentificationAttributes) {
            Set<Long> foundUserIds = adapter.getUserIdsByAttributeValue(attrName, userIdentifier);
            if (foundUserIds == null || foundUserIds.isEmpty()) {
                log.debug("No user IDs found for identifier '{}' and attribute '{}'",
                    userIdentifier, attrName);
                continue;
            }
            perunUserIds.addAll(foundUserIds);
        }

        if (perunUserIds.isEmpty()) {
            log.debug("User with identifier '{}' not found", userIdentifier);
            throw new UserNotFoundException("User with identifier '" + userIdentifier + "' not found");
        }

        if (perunUserIds.size() > 1) {
            log.warn("Multiple users found with identifier '{}' - user IDS: '{}'", userIdentifier, perunUserIds);
            throw new UserNotUniqueException("More than one user found for identifier '" +
                userIdentifier + '\'');
        }
        return perunUserIds.iterator().next();
    }

}
