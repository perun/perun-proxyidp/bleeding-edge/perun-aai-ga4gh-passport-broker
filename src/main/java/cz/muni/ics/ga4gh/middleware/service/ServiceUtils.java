package cz.muni.ics.ga4gh.middleware.service;

import cz.muni.ics.ga4gh.base.model.Ga4ghPassport;
import cz.muni.ics.ga4gh.base.model.Ga4ghPassportClaim;
import cz.muni.ics.ga4gh.base.model.Ga4ghTokenAuthorization;
import java.net.URI;
import java.time.LocalDateTime;
import java.util.UUID;
import lombok.NonNull;

public class ServiceUtils {

    public static Ga4ghPassport getGa4ghPassport(@NonNull Ga4ghTokenAuthorization accessToken,
                                                 @NonNull Ga4ghPassportClaim passportClaim,
                                                 @NonNull URI jku,
                                                 @NonNull String issuer,
                                                 @NonNull JWTSigningService signingService)
    {
        LocalDateTime firstVisaExp = passportClaim.getFirstVisaExp();
        if (firstVisaExp == null) {
            firstVisaExp = LocalDateTime.now();
        }
        Ga4ghPassport passport = new Ga4ghPassport();
        passport.setJku(jku);
        passport.setIss(issuer);
        passport.setSub(accessToken.getSub());
        passport.setAud(accessToken.getAud());
        passport.setIat(LocalDateTime.now());
        passport.setExp(firstVisaExp);
        passport.setJti(UUID.randomUUID().toString());
        passport.setGa4ghPassportV1(passportClaim);
        passport.generateSignedJwt(signingService);
        return passport;
    }

}
