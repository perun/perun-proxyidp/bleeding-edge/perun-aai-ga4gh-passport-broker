package cz.muni.ics.ga4gh.middleware.facade.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import cz.muni.ics.ga4gh.base.model.Ga4ghPassport;
import cz.muni.ics.ga4gh.base.model.Ga4ghPassportClaim;
import cz.muni.ics.ga4gh.base.model.Ga4ghTokenAuthorization;
import cz.muni.ics.ga4gh.middleware.facade.Ga4ghBrokerFacade;
import cz.muni.ics.ga4gh.middleware.service.Ga4ghBrokerService;
import cz.muni.ics.ga4gh.middleware.service.exception.UserNotFoundException;
import cz.muni.ics.ga4gh.middleware.service.exception.UserNotUniqueException;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class Ga4GhBrokerFacadeImpl implements Ga4ghBrokerFacade {

    private final Ga4ghBrokerService ga4ghBrokerService;

    @Autowired
    public Ga4GhBrokerFacadeImpl(Ga4ghBrokerService ga4ghBrokerService) {
        this.ga4ghBrokerService = ga4ghBrokerService;
    }

    @Override
    public String getGa4ghPassport(@NonNull Ga4ghTokenAuthorization ga4ghAccessToken)
        throws UserNotFoundException, UserNotUniqueException
    {
        Ga4ghPassport passport = ga4ghBrokerService.getGa4ghPassportTokenStyle(ga4ghAccessToken);
        if (passport == null) {
            return "";
        } else {
            return passport.serialize();
        }
    }

    @Override
    public JsonNode getGa4ghPassportClaim(@NonNull String userIdentifier)
        throws UserNotFoundException, UserNotUniqueException
    {
        if (!StringUtils.hasText(userIdentifier)) {
            throw new IllegalArgumentException("User identifier cannot be empty");
        }

        Ga4ghPassportClaim passport = ga4ghBrokerService.getGa4ghPassportUserinfoStyle(userIdentifier);
        if (passport == null) {
            return JsonNodeFactory.instance.nullNode();
        } else {
            return passport.asClaim();
        }
    }

}
