package cz.muni.ics.ga4gh.middleware.broker.impl;

import static cz.muni.ics.ga4gh.base.model.Ga4ghPassportVisa.BY_SYSTEM;

import cz.muni.ics.ga4gh.base.model.Affiliation;
import cz.muni.ics.ga4gh.base.model.Ga4ghPassportVisa;
import cz.muni.ics.ga4gh.base.model.VisaV1Type;
import cz.muni.ics.ga4gh.base.properties.PerunBrokerInstanceProperties;
import cz.muni.ics.ga4gh.middleware.broker.helper.PassportAssemblyContext;
import cz.muni.ics.ga4gh.middleware.service.JWSValidationService;
import cz.muni.ics.ga4gh.middleware.service.JWTSigningService;
import cz.muni.ics.ga4gh.persistence.database.repository.VisaEntityRepository;
import cz.muni.ics.ga4gh.persistence.perun.adapters.PerunAdapterRpc;
import java.net.URI;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ElixirGa4ghBroker extends AbstractPerunBasedGa4ghBroker {

    private static final String ELIXIR_ORG_URL = "https://elixir-europe.org/";

    public ElixirGa4ghBroker(@NonNull PerunBrokerInstanceProperties brokerInstanceProperties,
                             @NonNull PerunAdapterRpc adapter,
                             @NonNull URI jku,
                             @NonNull JWTSigningService signingService,
                             @NonNull JWSValidationService validationService,
                             VisaEntityRepository visaEntityRepository)
    {
        super(brokerInstanceProperties, adapter, jku, signingService, validationService, visaEntityRepository);
    }

    @Override
    protected String getSourceOrgUrl() {
        return ELIXIR_ORG_URL;
    }

    @Override
    protected void addAffiliationAndRoles(@NonNull PassportAssemblyContext ctx)
    {
        logAddingVisas(VisaV1Type.AFFILIATION_AND_ROLE);
        if (!super.isCommunityMember(ctx.getPerunUserId())) {
            log.debug("User is not member of the ELIXIR community, not adding any ELIXIR affiliation visa");
            return;
        }
        Affiliation affiliate = new Affiliation(ELIXIR_ORG_URL,
            "affiliate@elixir-europe.org", System.currentTimeMillis() / 1000L);
        Ga4ghPassportVisa affiliateVisa = createAffiliationVisa(
            affiliate, ctx.getSubject(), ctx.getPerunUserId(), BY_SYSTEM);
        if (affiliateVisa != null) {
            ctx.getResultVisas().add(affiliateVisa);
            logAddedVisa(affiliateVisa);
        }
    }

}
