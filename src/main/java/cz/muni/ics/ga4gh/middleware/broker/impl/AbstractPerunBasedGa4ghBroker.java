package cz.muni.ics.ga4gh.middleware.broker.impl;

import static cz.muni.ics.ga4gh.base.model.Ga4ghPassportVisa.BY_PEER;
import static cz.muni.ics.ga4gh.base.model.Ga4ghPassportVisa.BY_SELF;
import static cz.muni.ics.ga4gh.base.model.Ga4ghPassportVisa.BY_SYSTEM;

import com.fasterxml.jackson.core.JsonProcessingException;
import cz.muni.ics.ga4gh.base.Utils;
import cz.muni.ics.ga4gh.base.model.Affiliation;
import cz.muni.ics.ga4gh.base.model.ClaimRepositoryHeader;
import cz.muni.ics.ga4gh.base.model.Ga4ghClaimRepository;
import cz.muni.ics.ga4gh.base.model.Ga4ghPassportVisa;
import cz.muni.ics.ga4gh.base.model.VisaV1Type;
import cz.muni.ics.ga4gh.base.properties.Ga4ghClaimRepositoryProperties;
import cz.muni.ics.ga4gh.base.properties.PerunBrokerInstanceProperties;
import cz.muni.ics.ga4gh.middleware.broker.PerunBasedGa4ghBroker;
import cz.muni.ics.ga4gh.middleware.broker.helper.ExternalRepositoryUtils;
import cz.muni.ics.ga4gh.middleware.broker.helper.PassportAssemblyContext;
import cz.muni.ics.ga4gh.middleware.broker.helper.PassportAssemblyContext.PassportAssemblyContextBuilder;
import cz.muni.ics.ga4gh.middleware.broker.helper.VisaAssemblyParameters;
import cz.muni.ics.ga4gh.middleware.broker.helper.VisaCreatingUtils;
import cz.muni.ics.ga4gh.middleware.service.JWSValidationService;
import cz.muni.ics.ga4gh.middleware.service.JWTSigningService;
import cz.muni.ics.ga4gh.persistence.database.entity.VisaEntity;
import cz.muni.ics.ga4gh.persistence.database.repository.VisaEntityRepository;
import cz.muni.ics.ga4gh.persistence.perun.adapters.PerunAdapterRpc;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.sql.Timestamp;
import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.client.InterceptingClientHttpRequestFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

@Slf4j
public abstract class AbstractPerunBasedGa4ghBroker implements PerunBasedGa4ghBroker {

    private static final String BONA_FIDE_URL = "https://doi.org/10.1038/s41431-018-0219-y";

    private static final String FACULTY_AT = "faculty@";

    private final List<Ga4ghClaimRepository> claimRepositories = new ArrayList<>();

    private final String visaIssuer;

    private final URI visaIssuerJku;

    private final PerunAdapterRpc perunAdapter;

    private final JWTSigningService signingService;
    private final JWSValidationService validationService;

    private final String uesAffiliationsAttributeName;

    private final String groupAffiliationsAttr;

    private final String groupAffiliationsOrgUrlAttr;

    private final Long groupAffiliationVoId;

    private final Long groupAffiliationRootGroupId;

    private final String groupAffiliationsGroupMemberExpirationAttr;

    private final String userIdAttr;

    private final String bonaFideStatusAttr;

    private final String bonaFideStatusREMSAttr;

    private final Long termsAndPoliciesGroupId;

    private final Long voId;

    private final VisaEntityRepository visaEntityRepository;

    @Getter(onMethod_= {@Override})
    private final String name;

    public AbstractPerunBasedGa4ghBroker(@NonNull PerunBrokerInstanceProperties brokerInstanceProperties,
                                         @NonNull PerunAdapterRpc perunAdapter,
                                         @NonNull URI visaIssuerJku,
                                         @NonNull JWTSigningService signingService,
                                         @NonNull JWSValidationService validationService,
                                         VisaEntityRepository visaEntityRepository)
    {
        this.name = brokerInstanceProperties.getName();
        this.perunAdapter = perunAdapter;
        this.visaIssuer = brokerInstanceProperties.getVisaIssuer();
        this.visaIssuerJku = visaIssuerJku;
        this.signingService = signingService;
        this.validationService = validationService;
        this.userIdAttr = brokerInstanceProperties.getIdentifierAttribute();
        this.voId = brokerInstanceProperties.getMembershipVoId();
        // affiliations
        this.uesAffiliationsAttributeName = brokerInstanceProperties.getUesAffiliationsAttr();
        // group affiliations
        this.groupAffiliationsAttr = brokerInstanceProperties.getGroupAffiliationsAttr();
        this.groupAffiliationVoId = brokerInstanceProperties.getGroupAffiliationsVoId();
        this.groupAffiliationsOrgUrlAttr = brokerInstanceProperties.getGroupAffiliationsOrgUrlAttr();
        this.groupAffiliationRootGroupId = brokerInstanceProperties.getGroupAffiliationsRootGroupId();
        this.groupAffiliationsGroupMemberExpirationAttr = brokerInstanceProperties.getGroupAffiliationsGroupMemberExpirationAttr();
        // researcher status
        this.bonaFideStatusAttr = brokerInstanceProperties.getBonaFideStatusAttr();
        this.bonaFideStatusREMSAttr = brokerInstanceProperties.getBonaFideStatusRemsAttr();
        this.termsAndPoliciesGroupId = brokerInstanceProperties.getTermsAndPoliciesGroupId();
        // repositories
        this.visaEntityRepository = visaEntityRepository;
        initializeClaimRepositories(brokerInstanceProperties, claimRepositories);
    }

    @Override
    public final List<Ga4ghPassportVisa> constructGa4ghPassportVisas(@NonNull Long userId) {
        if (!satisfiesConditionsForGeneratingVisas(userId)) {
            return new ArrayList<>();
        }
        PassportAssemblyContextBuilder ctxBuilder = PassportAssemblyContext.builder();

        fillContextBuilderHook(userId, ctxBuilder);

        PassportAssemblyContext ctx = ctxBuilder.build();

        addAffiliationAndRoles(ctx);
        addAcceptedTermsAndPolicies(ctx);
        addResearcherStatus(ctx);
        addControlledAccessGrants(ctx);
        addLinkedIdentities(ctx);
        addLocallyStoredVisas(ctx);

        return ctx.getResultVisas();
    }

    protected boolean satisfiesConditionsForGeneratingVisas(@NonNull Long userId) {
        return isCommunityMember(userId);
    }

    protected void addAffiliationAndRoles(@NonNull PassportAssemblyContext ctx) {
        logAddingVisas(VisaV1Type.AFFILIATION_AND_ROLE);
        if (ctx.getExternalAffiliations() != null) {
            for (Affiliation aff : ctx.getExternalAffiliations()) {
                Ga4ghPassportVisa visa = createAffiliationVisa(
                    aff, ctx.getSubject(), ctx.getPerunUserId(), BY_SYSTEM);
                if (visa != null) {
                    ctx.getResultVisas().add(visa);
                    logAddedVisa(visa);
                }
            }
        }
        if (ctx.getLocalAffiliations() != null) {
            for (Affiliation aff : ctx.getLocalAffiliations()) {
                Ga4ghPassportVisa visa = createAffiliationVisa(
                    aff, ctx.getSubject(), ctx.getPerunUserId(), BY_PEER);
                if (visa != null) {
                    ctx.getResultVisas().add(visa);
                    logAddedVisa(visa);
                }
            }
        }
    }

    protected void fillContextBuilderHook(@NonNull Long userId, @NonNull PassportAssemblyContextBuilder ctxBuilder) {
        String communityIdentifier = getUserSub(userId);

        List<Ga4ghPassportVisa> controlledAccessGrantsFromRepositories = new ArrayList<>();
        Set<String> linkedIdentities = new HashSet<>();

        ExternalRepositoryUtils.callExternalRepositories(
            communityIdentifier,
            claimRepositories,
            controlledAccessGrantsFromRepositories,
            linkedIdentities,
            validationService
        );

        List<Ga4ghPassportVisa> localDbVisas = fetchLocallyStoredVisas(
            communityIdentifier, linkedIdentities);

        ctxBuilder
            .perunAdapter(perunAdapter)
            .perunUserId(userId)
            .subject(communityIdentifier)
            .now(Instant.now().getEpochSecond())
            .externalAffiliations(getUserExtSourcesAffiliations(userId))
            .localAffiliations(getManuallyAssignedAffiliations(userId))
            .controlledAccessGrants(controlledAccessGrantsFromRepositories)
            .locallyStoredVisas(localDbVisas)
            .linkedIdentities(linkedIdentities);
    }

    protected List<Ga4ghPassportVisa> fetchLocallyStoredVisas(@NonNull String communityIdentifier,
                                                              @NonNull Set<String> linkedIdentities)
    {
        List<Ga4ghPassportVisa> localVisas = new ArrayList<>();
        if (visaEntityRepository != null) {
            Set<VisaEntity> visas = visaEntityRepository.findAllByUserIdAndExpAfter(
                communityIdentifier, LocalDateTime.now());
            if (visas != null && !visas.isEmpty()) {
                for (VisaEntity visa: visas) {
                    Ga4ghPassportVisa parsedVisa;
                    try {
                        parsedVisa = Utils.parseVisa(visa.getJwt());
                    } catch (ParseException | JsonProcessingException e) {
                        log.warn("Could not parse visa from '{}'", visa);
                        continue;
                    }

                    Utils.verifyVisa(parsedVisa, validationService);
                    if (!parsedVisa.isVerified()) {
                        log.warn("Could not verify visa '{}'", visa);
                        continue;
                    }
                    localVisas.add(parsedVisa);
                    linkedIdentities.add(visa.getLinkedIdentity());
                }
            }
        }
        return localVisas;
    }

    protected void addAcceptedTermsAndPolicies(@NonNull PassportAssemblyContext ctx) {
        VisaV1Type type = VisaV1Type.ACCEPTED_TERMS_AND_POLICIES;
        logAddingVisas(type);
        if (termsAndPoliciesGroupId == null) {
            log.debug("Group ID for accepted terms and policies is not defined, not adding any {} visas", type);
            return;
        }

        boolean userInGroup = perunAdapter.isUserInGroup(ctx.getPerunUserId(), termsAndPoliciesGroupId);
        if (!userInGroup) {
            log.debug("User is not in the group representing terms and policies approval, not adding any {} visas", type);
            return;
        }

        long asserted = ctx.getNow();
        long expires = Utils.getExpires(asserted, 100L);
        addVisaWithBonaFideUrlValue(ctx, type, asserted, expires, BY_SELF, getSourceOrgUrl());
    }

    protected void addControlledAccessGrants(@NonNull PassportAssemblyContext ctx) {
        VisaV1Type type = VisaV1Type.CONTROLLED_ACCESS_GRANTS;
        logAddingVisas(type);
        List<Ga4ghPassportVisa> controlledAccessGrants = ctx.getControlledAccessGrants();
        if (controlledAccessGrants == null || controlledAccessGrants.isEmpty()) {
            log.debug("No external {} visas available, not adding any {} visas", type, type);
            return;
        }
        ctx.getResultVisas().addAll(controlledAccessGrants);
    }

    private void addLocallyStoredVisas(@NonNull PassportAssemblyContext ctx) {
        List<Ga4ghPassportVisa> locallyStoredVisas = ctx.getLocallyStoredVisas();
        if (locallyStoredVisas == null || locallyStoredVisas.isEmpty()) {
            log.debug("No locally stored visas visas available, not adding any visas here");
            return;
        }
        log.debug("Added locally stored visas '{}'", locallyStoredVisas);
        ctx.getResultVisas().addAll(locallyStoredVisas);
    }

    protected void addLinkedIdentities(@NonNull PassportAssemblyContext ctx) {
        VisaV1Type type = VisaV1Type.LINKED_IDENTITIES;
        logAddingVisas(type);
        Set<String> externalLinkedIdentities = ctx.getLinkedIdentities();
        if (externalLinkedIdentities == null || externalLinkedIdentities.isEmpty()) {
            log.debug("No external {} visas available, not adding any {} visas", type, type);
            return;
        }
        for (String identity: externalLinkedIdentities) {
            Ga4ghPassportVisa visa = VisaCreatingUtils.createVisa(
                visaIssuer,
                visaIssuerJku,
                signingService,
                VisaAssemblyParameters.builder()
                    .type(type)
                    .sub(ctx.getSubject())
                    .userId(ctx.getPerunUserId())
                    .value(identity)
                    .source(getSourceOrgUrl())
                    .by(BY_SYSTEM)
                    .asserted(ctx.getNow())
                    .expires(Utils.getOneYearExpires(ctx.getNow()))
                    .conditions(null)
                    .build()
            );
            if (visa != null) {
                ctx.getResultVisas().add(visa);
                logAddedVisa(visa);
            }
        }
    }

    protected void addResearcherStatus(@NonNull PassportAssemblyContext ctx)
    {
        logAddingVisas(VisaV1Type.RESEARCHER_STATUS);
        //TODO: uncomment this when we want to enforce both conditions - accepted terms, researcher status to actually release the researcher status
        //String type = TYPE_RESEARCHER_STATUS;
        //logAddingVisas(type);

        //boolean userInGroup = adapter.isUserInGroup(ctx.getPerunUserId(), termsAndPoliciesGroupId);
        //if (!userInGroup) {
        //    log.debug("User is not in the group representing terms and policies approval, not adding any {} visas", type);
        //    return;
        //}
        addResearcherStatusFromRemsBonaFideAttribute(ctx);
        addResearcherStatusFromAffiliation(ctx);
    }

    protected Set<Affiliation> getUserExtSourcesAffiliations(Long userId) {
        if (!StringUtils.hasText(uesAffiliationsAttributeName)) {
            log.warn("Affiliations attribute or orgUrl attribute is not defined");
            return new HashSet<>();
        } else {
            return perunAdapter.getUserExtSourcesAffiliations(userId, uesAffiliationsAttributeName);
        }
    }

    protected Set<Affiliation> getManuallyAssignedAffiliations(Long userId) {
        Set<Affiliation> result = new HashSet<>();
        if (!StringUtils.hasText(groupAffiliationsAttr)) {
            log.warn("Group affiliations attribute is not set, skipping");
        } else if (!StringUtils.hasText(groupAffiliationsOrgUrlAttr)) {
            log.warn("Group affiliations organization URL attribute is not set, skipping");
        } else if (!StringUtils.hasText(groupAffiliationsGroupMemberExpirationAttr)) {
            log.warn("Group affiliations group member expiration attribute is not set, skipping");
        } else if (groupAffiliationVoId == null) {
            log.warn("Group affiliations VO ID is not set, skipping");
        } else if (groupAffiliationRootGroupId == null) {
            log.warn("Group affiliations root Group ID is not set, skipping");
        } else {
            result.addAll(
                perunAdapter.getGroupAffiliations(
                    userId,
                    groupAffiliationVoId,
                    groupAffiliationRootGroupId,
                    groupAffiliationsAttr,
                    groupAffiliationsOrgUrlAttr,
                    groupAffiliationsGroupMemberExpirationAttr
                )
            );
        }
        return result;
    }

    protected void addResearcherStatusFromRemsBonaFideAttribute(PassportAssemblyContext ctx) {
        VisaV1Type type = VisaV1Type.RESEARCHER_STATUS;
        log.debug("Adding {} visa (from REMS bona fide status)", type);
        if (!StringUtils.hasText(bonaFideStatusREMSAttr)) {
            log.debug("REMS bonaFideStatus attribute is not defined, not adding any {} visas (from REMS bona fide status)", type);
            return;
        }

        String elixirBonaFideStatusREMSCreatedAt = perunAdapter.getUserAttributeCreatedAt(
            ctx.getPerunUserId(), bonaFideStatusREMSAttr);
        if (elixirBonaFideStatusREMSCreatedAt == null) {
            return;
        }

        long asserted = Timestamp.valueOf(elixirBonaFideStatusREMSCreatedAt).getTime() / 1000L;
        long expires = Utils.getOneYearExpires(asserted);

        addVisaWithBonaFideUrlValue(ctx, type, asserted, expires, BY_PEER, getSourceOrgUrl());
    }

    protected void addResearcherStatusFromAffiliation(PassportAssemblyContext ctx) {
        VisaV1Type type = VisaV1Type.RESEARCHER_STATUS;
        log.debug("Adding {} visa (from affiliations)", type);
        if (ctx.getExternalAffiliations() == null || ctx.getExternalAffiliations().isEmpty()) {
            log.debug("No affiliations available, not adding any {} visas (from affiliations)", type);
            return;
        }

        Set<Affiliation> affiliations = new HashSet<>();
        if (ctx.getExternalAffiliations() != null) {
            affiliations.addAll(ctx.getExternalAffiliations());
        }
        if (ctx.getLocalAffiliations() != null) {
            affiliations.addAll(ctx.getLocalAffiliations());
        }
        for (Affiliation affiliation: affiliations) {
            if (!StringUtils.startsWithIgnoreCase(affiliation.getValue(), FACULTY_AT)) {
                continue;
            }

            String source = affiliation.getSource();
            long asserted = affiliation.getAsserted();
            long expires = Utils.getOneYearExpires(asserted);
            addVisaWithBonaFideUrlValue(ctx, type, asserted, expires, BY_SYSTEM, source);
        }
    }

    private void addVisaWithBonaFideUrlValue(PassportAssemblyContext ctx,
                                             VisaV1Type type,
                                             long asserted,
                                             long expires,
                                             String by,
                                             String source) {
        Ga4ghPassportVisa visa = VisaCreatingUtils.createVisa(
            visaIssuer,
            visaIssuerJku,
            signingService,
            VisaAssemblyParameters.builder()
                .type(type)
                .sub(ctx.getSubject())
                .userId(ctx.getPerunUserId())
                .value(BONA_FIDE_URL)
                .source(source)
                .by(by)
                .asserted(asserted)
                .expires(expires)
                .conditions(null)
                .build()
        );
        if (visa != null) {
            ctx.getResultVisas().add(visa);
            logAddedVisa(visa);
        }
    }

    protected String getUserSub(Long userId) {
        return perunAdapter.getUserSub(userId, userIdAttr);
    }

    protected boolean isCommunityMember(Long userId) {
        if (voId == null) {
            return true;
        }
        return perunAdapter.isUserInVo(userId, voId);
    }

    protected final void logAddingVisas(VisaV1Type type) {
        log.info("Adding '{}' visas", type);
    }

    protected final void logAddedVisa(Ga4ghPassportVisa visa) {
        log.debug("Added '{}' visa with value '{}'",
            visa.getGa4ghVisaV1().getType(), visa.getGa4ghVisaV1().getValue());
    }

    protected final Ga4ghPassportVisa createAffiliationVisa(Affiliation affiliation,
                                                          String subject,
                                                          Long userId,
                                                          String by)
    {
        return VisaCreatingUtils.createVisa(
            visaIssuer,
            visaIssuerJku,
            signingService,
            VisaAssemblyParameters.builder()
                .type(VisaV1Type.AFFILIATION_AND_ROLE)
                .sub(subject)
                .userId(userId)
                .value(affiliation.getValue())
                .source(affiliation.getSource())
                .by(by)
                .asserted(affiliation.getAsserted())
                .expires(Utils.getOneYearExpires(affiliation.getAsserted()))
                .conditions(null)
                .build()
        );
    }

    private void initializeClaimRepositories(
        @NonNull PerunBrokerInstanceProperties brokerInstanceProperties,
        @NonNull List<Ga4ghClaimRepository> claimRepositories)
    {
        for (Ga4ghClaimRepositoryProperties ga4ghClaimRepositoryProperties:
            brokerInstanceProperties.getPassportRepositories())
        {
            ExternalRepositoryUtils.initializeClaimRepository(
                ga4ghClaimRepositoryProperties,
                claimRepositories,
                brokerInstanceProperties.getPassportRepositoriesTimeout()
            );
            ExternalRepositoryUtils.initializeRepositoryValidationKeys(
                ga4ghClaimRepositoryProperties.getName(),
                ga4ghClaimRepositoryProperties.getJwks(),
                validationService
            );
        }
    }

    protected abstract String getSourceOrgUrl();

}
