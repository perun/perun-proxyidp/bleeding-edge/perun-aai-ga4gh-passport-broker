package cz.muni.ics.ga4gh.middleware.broker;

import cz.muni.ics.ga4gh.base.model.Ga4ghPassportVisa;
import java.util.List;

public interface MockOidcGa4ghBroker {

    List<Ga4ghPassportVisa> constructGa4ghPassportVisas(String userId);

}
