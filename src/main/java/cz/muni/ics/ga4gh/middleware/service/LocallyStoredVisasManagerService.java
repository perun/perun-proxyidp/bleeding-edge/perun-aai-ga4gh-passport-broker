package cz.muni.ics.ga4gh.middleware.service;

import java.util.Set;
import javax.validation.constraints.NotNull;

public interface LocallyStoredVisasManagerService {

    void persistVisas(@NotNull String username, @NotNull Set<String> visas);

}
